# SOAP docs

::: tip
See also: automatically generated [PHP Swagger Docs](/php-reference)
:::

This page provides detailed information on automatic data exchange procedure between a Shipper’s or a Forwarding Agent’s information system and the TK’Blue platform.

Once programming is done, it is advised to test it on the TK’Blue Sandbox website : [https://sandbox-notation.tkblueagency.com](https://sandbox-notation.tkblueagency.com/en/). The identifiers are provided by the TK’Blue team.

Upon completing the tests, the following link https://sandbox-notation.tkblueagency.com must be replaced by [https://www-notation.tkblueagency.com](https://www-notation.tkblueagency.com) and tkblue_sandbox.wsdl by tkblue.wsdl

The TK’Blue API has been developped using Soap Client/Server (Simple Object Access Protocol) technology.

For more information on programming, visit [http://fr.wikibooks.org/wiki/Programmation_PHP/Exemples/webService](http://fr.wikibooks.org/wiki/Programmation_PHP/Exemples/webService) and then [http://soapclient.com/soaptest.html](http://soapclient.com/soaptest.html) to test the server.

Find on this link all the functions of the TK’Blue API: [https://www.tkblueagency.com/api/reference/](/php-reference/#documentation-for-models)

## Authentication and Authorization

### Identification With Token Recovery

The authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function This function has 2 input parameters, the member’s email and sha2 encrypted password. This function returns an authentication token to be used later in calls to other functions.

#### Input parameters

| Field name | Type   | Description                                           |
| ---------- | ------ | ----------------------------------------------------- |
| Email      | String | Member’s login ID on the platform                     |
| Password   | String | sha2 encryption of the login password on the platform |

#### Output parameters

The “authentication token” field is a returned to be used in further calls.

#### Example of a code

@[code](./codes/php/identificationAvecRécupérationDUnJeton.php)

## Pagination

The "total" parameter returned by a function allows to know how many results are available. When it is greater than 100, it is imperative to specify the page number to be returned and to make successive calls to the function by incrementing this page number until it is used up.

## Errors

| Message                            | Code   | Description                                                       |
| ---------------------------------- | ------ | ----------------------------------------------------------------- |
| wrong cotisation rights            | 200 OK | Your subscription plan does not allow you to do this action       |
| unauthorized:Identifiant incorrect | 200 OK | Login incorrect                                                   |
| wrong identification               | 200 OK | Login incorrect                                                   |
| incorrect transport mode           | 200 OK | Function `getCarrierStatistics` is missing the `option` parameter |
| error                              | 200 OK | General error                                                     |

## Importing flows in the TK'Blue platform

The import procedure consists of 3 steps: • Identification with token recovery, • Sending data, • Recovery of possible errors.

The authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function and providing the member’s email and sha1 encrypted password. This function returns an authentication token.

Then call the [**sendChargeurDeclaration**](/php-reference/#documentation-for-models) function with the email previously provided, the returned authentication token, a string to describe the selected transfer options and a string containing the transport services to import whose format depends on the selected options.

Once the data has been imported, a feedback is given on its quality, providing the number of imported flows (Import) and the number of valid flows (Valid). If the number returned “Imported” is not equal to the number “Valid”, it means that benefits have been imported incompletely. It will not be possible to take these flows into account. These errors can be fixed on the platform, but it is planned to obtain information by calling the [**getLastErrorList**](/php-reference/#documentation-for-models) function.

[**Requires authentication**](#authentication-and-authorization)

### Data Dispatch

Once the authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function, it is necessary to call the [**sendChargeurDeclaration**](/php-reference/#documentation-for-models) function.

This function has 4 input parameters: the member’s email, the authentication token, a transfer options field and a transport services field. It indicates in return the number of services correctly imported.

#### Input parameters

| Field name           | Type   | Description                              |
| -------------------- | ------ | ---------------------------------------- |
| Email                | String | Member’s login ID on the platform        |
| Authentication token | String | Returned by the ConnectNotation function |
| Transfer options     | String | Detailed below                           |
| Transport services   | String | Detailed below, in CSV format            |

The field **“transfer options”** is a string of characters that will be analyzed by a PHP call of the PARSE QUERY type, i. e. it consists of one or more options of the form “option_name1=option_value1&option_name2=option_value2…..”

The possible options are described below:

| Field name     | Type   | Description                                                                                                                                                                                                                                                          | Values               |
| -------------- | ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------- |
| csvDelimiter   | String | Indication of the character delimiting the fields, by default the semicolon is used                                                                                                                                                                                  |                      |
| csvHeader      | Int    | Default value 0 (column headers are not to be included), otherwise indicate 1 if they are                                                                                                                                                                            | 0 or 1               |
| fileFormatName | String | Name of the export format previously created in the TK’Blue space and allowing to specify a different order for columns, special units and additional data fields (in particular Origin and Destination)                                                             |                      |
| uniqueRef      | Int    | Indication of the presence of a unique reference and use of the Deduplication function. Default value 1: each flow has a unique reference and deduplication must be performed when several flows have the same reference. Otherwise, value 0 to avoid deduplication. | 0 or 1               |
| year           | Int    | Indication of the rating year concerned. Default value: the current year at the time the flows are imported. To import flows over another year, the corresponding year must be indicated.                                                                            | 2015 or 2016 or 2017 |
| postponed      | Int    | Obsolete parameter, always considered equal to 1 in order to allow the storage of flows for a faster return of function, the processing is then done in delayed mode.                                                                                                | 0 or 1               |

**Examples:**

• csvDelimiter=, • csvDelimiter=,&fileFormatName=nomformat

**String of characters containing transport services**

If field “fileFormatName” is empty, each CSV row must contains the following fields in correct order:

| Field name          | Nature       | Type             | Description                                                       | Values                                                                                                               |
| ------------------- | ------------ | ---------------- | ----------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| Ref                 | O            | String (20c max) | Internal reference to the TMS identifying a segment of a service  | Ex: ”C13123-002“                                                                                                     |
| idCTransporteur     | M            | String           | Intracommunity VAT number\*                                       | Ex: ”FR01378901946”                                                                                                  |
| idModality          | C            | String           | Title of the mode of transport\*                                  | ”Urban Road” , ”Interurban Road” , ”Rail” , “River” , ”Short Sea Shipping” , ”Deep Sea” , ”Air” , ”Forwarding Agent” |
| idFloteTransporteur | C            | String           | Carrier fleet identifier\*                                        | Ex: ”FPTRANPO_90_1”                                                                                                  |
| FloteSpec           | M            | String           | Specificity of the fleet                                          | ”or ”Reefer”                                                                                                         |
| DateTransporteur    | M            | String YYY-MM-DD | Date of the service                                               |                                                                                                                      |
| WeightPos           | M            | Int, en kg       | weight                                                            |                                                                                                                      |
| KmPos               | C            | Int, en km       | Distance                                                          |                                                                                                                      |
| CO2                 | O            | Int, en gCO2     | CO2 information                                                   |                                                                                                                      |
| CO2Level            | O            | Int              | Level of information CO2                                          | From 1 to 4                                                                                                          |
| RefAgr              | O&lt;/td&gt; | String (20c max) | Internal reference to the TMS identifying a multi-segment service | Ex: ”C13123”                                                                                                         |

**Nature of the fields**

Fields with the nature M (mandatory) are mandatory. The absence of such a field or a nil value will result in the benefit being processed in error.

Those whose nature is C (correspondence) can be omitted if the correspondence settings allow them to be determined. Otherwise, the absence of such a field or a nil value will result in the benefit being processed in error.

Those whose nature is O (optional) may be absent or filled in with a null value, without causing misleading processing.

Fields marked with an asterisk can be filled in using the correspondence settings.

**CO2 Information**

The fields concerning CO2 are to be filled in only if the transport organiser or shipper wishes to manage the calculation of CO2 information himself. In this case no automatic calculation of CO2 information will be made.

On the other hand, if zero values are transmitted for these fields, the CO2 information and the level of information will be automatically calculated based on the information collected by the carriers performing the transport services.

**Internal references**

The Ref and RefAgr fields are to be treated differently depending on whether the customer is a transport organiser or a shipper:

→ _For a freight forwarder:_ • The Ref field corresponds to the internal reference of the TMS. In the case of a service comprising several segments, each segment must be the subject of a separate data line and must therefore have its own unique reference. The RefAgr field then contains the internal reference to the TMS common to the entire service. When the transport service consists of only one segment, the Ref and RefAgr fields will be identical. • The transport organiser must communicate the value of RefAgr to its principal so that the latter can follow its CO2 information.

→ _For a shipper:_ • The Ref field corresponds to the internal reference of the TMS. In the case of a service with several segments, if each segment is directly entrusted to carriers, each segment must be the subject of a separate data line and must therefore have its own unique reference. The RefAgr field then contains the internal reference to the TMS common to the entire service. When the transport service comprises only one segment and is entrusted directly to a carrier, the Ref and RefAgr fields will be identical. • In the case of a service entrusted to a transport organiser, the possible splitting of the service into several segments is managed in the data flow of the transport organiser. The RefAgr field must contain the reference communicated by the transport organiser to the shipper in order to allow him to track his CO2 information.

**Mode of transport**

Special case of the shipper entrusting a service to a freight forwarder

The shipper can choose to specify “Forwarding Agent” as the mode of transport, so he does not need to specify the fleet identifier used. This will be particularly the case when the service entrusted to the transport organiser is split into several segments in its internal flow, which may correspond to different modes of transport, and to different carriers and fleet identifiers.

This will still be the case for a single-segment service, for which the shipper leaves the transport organiser entirely free to choose the most suitable mode of transport.

**Parameters of call**

Each row of CSV data must be separated by an end of line character and must contain the information previously described separated by the character defined in the csvDelimit option.

When using the fileFormatName parameter, the different fields must be ordered according to the import format specified by this parameter. Similarly, the units used must correspond to those defined in the specified import format.

#### Output parameters

The return of the[**sendChargeurDeclaration**](/php-reference/#documentation-for-models) function is a string in json format containing, for compatibility with previous versions, the total number of imported services (complete or incomplete), the total number of rejected services as well as the total number of imported services without missing information. Note: since processing is always carried out on a deferred basis, the total number of services imported without missing information will be artificially equal to the total number of services imported.

#### Example of a code

The import can be tested from a web page containing the following php code as an example:

@[code](./codes/php/envoiDesDonnées.php)

**Example of the content of the variable $\_POST\[‘list’\] with the choice of the CSV format:**

“4A”,”FR01378901946″,”Urban Road”,”FPgoodgnv-105-1″,””,”2013-04-07″,10, 40, “”,””,”4″ “4B”,”FR01378901946″,”Urban Road”,”FPTRANPO-90-1″,””,”2013-04-04″,10,40, “1203”,”3″,”4″

**Example of the returned string:**

\[{"Imported":2, "Rejected":0, "Valid":2}\]

### Management Of Deferred Processing

The [**getPostponedImportStatus**](/php-reference/#documentation-for-models) function has 3 input parameters: the member’s email, the token returned by the connection function and the deferred processing ID, and returns a string in JSON format, indicating the status of the deferred processing and the date and time corresponding to this status. The deferred processing identifier to be provided is the one that was previously returned when the postponed transfer option=1 was used.

#### Input parameters

| Field name                     | Type   | Description                                      |
| ------------------------------ | ------ | ------------------------------------------------ |
| Email                          | String | Member’s login ID on the platform                |
| Authentication token           | String | Returned by the ConnectNotation function         |
| Deferred processing identifier | Int    | Returned by the sendChargeurDeclaration function |

#### Output parameters

**getPostponedImportStatus** is a string in json format containing:

→ Status: one text among: • Import file format needs to be chosen: only for file repositories not using the webservice • First record needs to be checked: only for file repositories not using the webservice • Ready for process: This is the state in which the deferred processing generated by webservice is located just after the sendChargerDeclaration function • Processing: the first step of deferred processing is in progress • First pass import done: the second step of the deferred processing is in progress • Import done: deferred processing is complete

→ SatusDate: the date and time corresponding to the returned status

Example of return of the [**getPostponedImportStatus**](/php-reference/#documentation-for-models) function

```json
{ "Status": "Import done", "StatusDate": "2016-10-11 22:31:21" }
```

### Error Handling

The [**getLastErrorList**](/php-reference/#documentation-for-models) function has 2 input parameters: the member’s email and the token returned by the login function, and returns a string in JSON format, indicating on the one hand the number of services in error during the last import, and on the other hand the list of detected errors. Since the processing of an import is delayed, it is recommended not to call this function immediately after sending data.

#### Input parameters

| Field name           | Type   | Description                              |
| -------------------- | ------ | ---------------------------------------- |
| Email                | String | Member’s login ID on the platform        |
| Authentication token | String | Returned by the ConnectNotation function |

#### Output parameters

The return of the [**getLastErrorList**](/php-reference/#documentation-for-models) functionis a string in json format containing:

→ Error: the number of imported services causing a problem, → List: string in json format containing the details of an error: • Field: name of the field • How: description of the error • Value: field value • N: number of times the error was encountered

There may be more errors detected than benefits misunderstood since the same benefit may have been misled for several reasons. For each type of error returned, we find the name of the field, a comment explaining the rejection, the incorrect value transmitted as well as the number of benefits affected by this error.

#### Code examples

**Example of poorly imported services**

”4E”,”01378901946”,”Routier Urbain”,”FPgoodgnv-105-1”,””,”2013-04-07”,”10”, ”40” ”1E”,”FR01378901946”,”Routier Urbain”,”FPTRANPO-90-1”,””,”2013-04-04”,”10T”,”40”

→ “01378901946” is not a correct VAT number
→ “10T” is not a numerical field and should not contain the unit

**Example of return of the [getLastErrorList](/php-reference/#documentation-for-models)** function

```json
{
  "Error": "2",
  "List": [
    {
      "Field": "Weight",
      "How": "Weight non num\\u00e9rique",
      "Value": "10T",
      "N": "1"
    },
    {
      "Field": "FlotteLabel",
      "How": "Fleet without carrier",
      "Value": "FPgoodgnv-105-1",
      "N": "1"
    },
    {
      "Field": "VatNumberString",
      "How": "Unknown carrier",
      "Value": "01378901946",
      "N": "1"
    }
  ]
}
```

## Exporting flows from TK'Blue platform

The export procedure is carried out in 2 steps: • Identification with token recovery • Data recovery,

The authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function and providing the member’s email and sha1 encrypted password. This function returns an authentication token.

Then call the [**downloadChargeurDeclaration**](/php-reference/#documentation-for-models) function with the email previously provided, the returned authentication token and a string of characters to describe the chosen transfer options.

[**Requires authentication**](#authentication-and-authorization)

### Data Recovery

Once the authentication token obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function you must call the [**downloadChargeurDeclaration**](/php-reference/#documentation-for-models) function This function has 3 input parameters, the member’s email, the authentication token and a transfer option field. It returns information about the selected services.

#### Input parameters

| Field name           | Type   | Description                              |
| -------------------- | ------ | ---------------------------------------- |
| Email                | String | Member’s login ID on the platform        |
| Authentication token | String | Returned by the ConnectNotation function |
| Transfer options     | String | Detailed below                           |

The field **“transfer options”** is a string of characters that will be analyzed by a PHP call of type PARSE QUERY, i. e. it consists of one or more options of the form “option_name1=option_value1&option_name2=option_value2….” and one or more search criteria

The possible options are described below:

**Format options** The possible format options are:

| Field name     | Type   | Description                                                                                                                                                                                                                                                                                                             | Values                   |
| -------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ |
| dataType       | String | If this parameter is not specified, the JSON format will be used                                                                                                                                                                                                                                                        | JSON or CSV              |
| csvDelimiter   | String | Only for the dataType=CSV option. Indication of the character delimiting the fields, by default the semicolon is used.                                                                                                                                                                                                  |                          |
| csvHeader      | Int    | Only for the dataType=CSV option. Default value 0 : column headers are not to be included, otherwise indicate 1 if they are.                                                                                                                                                                                            | 0 or 1                   |
| fileFormatName | String | Name of the export format previously created in the loader area and allowing, in the case of a CSV format shipment, to specify a different order for columns, special units and additional data fields (in particular Origin and Destination)                                                                           |                          |
| pagenumber     | Int    | Page number in all search results when using a paged search. The selection range is specified by a page number and a number of results (linenumber) per page to be returned.                                                                                                                                            | The default value is 0   |
| linenumber     | Int    | Maximum number of benefits to be returned. The selection range is specified by a page number (pagenumber) and a number of results per page to be returned. The default value is 100, it is also the maximum possible value. To recover more than 100 services you must schedule successive calls and manage the paging. | The default value is 100 |

**Examples :**

• dataType=CSV&csvDelimiter=, • dataType=CSV&csvDelimiter=,&fileFormatName=nomformat • dataType=CSV&csvDelimiter=,&pagenumber=2&linenumber=10

**The search criteria**

The search criteria correspond to those available on the platform, in the declaration history.

| Field name | Type   | Correspondence      | Values                                                                                                                                |
| ---------- | ------ | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| Pb         | Int    | “Show only”         | 1 : Valid entries 2 : Incorrect entries 3 : Modifications                                                                             |
| minDate    | String | “Display only from” | ‘YYYY-MM-DD’                                                                                                                          |
| maxDate    | String | “Display only on”   | ‘YYYY-MM-DD’                                                                                                                          |
| CO2        | Int    | « CO2 monitoring»   | 0 : unknown -1 : known at any level 1 : level 1 2 : level 2 3 : level 3 4 : level 4                                                   |
| TKB        | Int    | “TK’T monitoring”   | 0 : unknown -1 : known at any level 1 : Provisional level 2 : Declared level 3 : Validated level                                      |
| VatNumber  | String | “Carrier”           | Ex : “FR01378901946” if the identifier is an intra-Community VAT number, “123456789B\|SG\|UEN” for a UEN-type identifier in Singapore |
| FieldLabel | String | “Search: field”     | ‘Ref’, ‘RefAgr’, ‘Origin’, ‘Destination’                                                                                              |
| FieldValue | String | “Search: value”     |                                                                                                                                       |
| Year       | Int    | “Year”              | y-2, y-1, y, y+1 where is the current year, at the time the export is launched                                                        |

**Dependency on criteria**

The criteria minDate and maxDate are to be compared to criterion Pb, and correspond to the dates of the service if criterion Pb is omitted or different from 3. they correspond to the dates of modifications if criterion Pb is 3.

Examples: • Year=2017&VatNumber=FR89123456789|FR|VAT • Year=2017&VatNumber=123456789B|SG|UEN • Pb=2&minDate=2013-06-01&maxDate=2013-12-31 • Pb=3&minDate=2013-06-01&maxDate=2013-08-31

The FieldValue criterion is to be compared to the FieldLabel criterion. The selection will focus on services whose FiledLabel field has the FieldValue value. Examples : • FieldLabel=RefAgr&FieldValue=Man1 • FieldLabel=Origin&FieldValue=Nice

#### Output parameters

The return of the [**downloadChargeurDeclaration**](/php-reference/#documentation-for-models) function is a string in json format containing 2 parameters :

| Field name | Type | Description                                                                                                                                                                 |
| ---------- | ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Total      | Int  | The total number of services concerned by the search criteria excluding pagination                                                                                          |
| data       | JSON | A JSON string containing the services returned in the chosen format (CSV or JSON), each service including the fields selected by the export format indicated in the options |

```json
{
  "total": "18",
  "data": [
    {
      "Ref": "4A",
      "DateTransport": "2013-04-07",
      "idModality": "Routier Urbain",
      "idFlotteTransporteur": "FPgoodgnv-105- 1",
      "FlotteSpec": "",
      "Km": "400",
      "Weight": "10000",
      "CO2": "120",
      "CO2Level": "2",
      "RefAgr": "4",
      "idTransporteur": "FR01378901946",
      "Origin": null,
      "Destination": null
    },
    {
      "Ref": "7001-9153",
      "DateTransport": "2013-04-25",
      "idModality": "Routier Interurbain",
      "idFlotteTransporteur": "",
      "FlotteSpec": "",
      "Km": "12",
      "Weight": "97",
      "CO2": "0",
      "CO2Level": "0",
      "RefAgr": "7001-9153",
      "idTransporteur": "",
      "Origin": null,
      "Destination": null
    },
    {
      "Ref": "7645-10002",
      "DateTransport": "2013-04-27",
      "idModality": "Routier Interurbain",
      "idFlotteTransporteur": "",
      "FlotteSpec": "Frigo",
      "Km": "12",
      "Weight": "107",
      "CO2": "0",
      "CO2Level": "0",
      "RefAgr": "7645-10002",
      "idTransporteur": "",
      "Origin": null,
      "Destination": null
    },
    ...
  ]
}
```

[**Supports pagination**](#pagination)

#### Example of a code

The export can be tested from a web page containing the following php code as an example:

@[code](./codes/php/ExporterFlux.php)

## Importing carrier and client matching settings

The import procedure is carried out in 2 steps: • Identification with token recovery • Sending data.

The authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function and providing the member’s email and sha1 encrypted password. This function returns an authentication token.

Then call the [**putCarrierCorrierCorrespondancyList**](/php-reference/#documentation-for-models) function with the email previously provided, the returned authentication token, a string to describe the transfer options chosen and a string containing information about the carriers to import whose format is predetermined.

If the number returned “Imported” is not equal to the number “Valid”, it means that imported information is in error.

[**Requires authentication**](#authentication-and-authorization)

### Data Dispatch

Once the authentication token obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function you must call the function [**putCarrierCorrespondacyList**](/php-reference/#documentation-for-models) function. This function has 4 input parameters, the member’s email, the authentication token, a transfer options field and a Carrier Correspondence field. It indicates in return the number of services correctly imported.

#### Input parameters

| Fiel name               | Type   | Description                              |
| ----------------------- | ------ | ---------------------------------------- |
| Email                   | String | Member’s login ID on the platform        |
| Authentication token    | String | Returned by the ConnectNotation function |
| Transfer options        | String | Detailed below                           |
| Correspondence Carriers | String | Detailed below                           |

The field **“transfer options”** is a string of characters that will be analyzed by a PHP call of type PARSE QUERY, i. e. it consists of one or more options of the form “option_name1=option_value1&option_name2=option_value2…”.

**The possible options are described below:**

| Field name     | Type   | Description                                                                                                                  | Values      |
| -------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------- | ----------- |
| dataType       | String | If this parameter is not specified, the JSON format will be use                                                              | JSON or CSV |
| csvDelimiter   | String | Only for the dataType=CSV option. Indication of the character delimiting the fields, by default the semicolon is used.       |             |
| csvHeader      | Int    | Only for the dataType=CSV option. Default value 0 : column headers are not to be included, otherwise indicate 1 if they are. | 0 or 1      |
| sendInvitation | Int    | Default value 1: Invitations are to be sent to carriers for which an email and a company name have been specified            | 0 or 1      |

**Examples:**

- dataType=CSV&csvDelimiter=,
- dataType=JSON

**String of characters containing the correspondences Carriers**

When the chosen format is JSON, for each service, the json structure must contain the fields:

| Field name           | Nature | Type   | Description                                                                                                        | Values                                                                                         |
| -------------------- | ------ | ------ | ------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------- |
| Name                 | M      | String | Carrier name as mentioned in flows                                                                                 | Ex: ”TRPT Valognes” or “F10XZ33”                                                               |
| TIN                  | M      | String | Carrier’s NIF identifier                                                                                           | Ex : “FR01378901946”                                                                           |
| idModality           | O      | String | Title of the mode of transport\*                                                                                   | ”Urban Road” , ”Interurban Road” , ”Rail” , “River” , ”Short Sea” , ”Air” , ”Forwarding Agent” |
| idFlotteTransporteur | O      | String | Carrier fleet identifier\*                                                                                         | Ex: ”FPTRANPO_90_1”                                                                            |
| FlotteSpec           | O      | String | Specificity of the fleet                                                                                           | Empty or “Reefer”                                                                              |
| idAdemeCO2           | O      | String | Reference of the CO2 level 1 index                                                                                 | A\|30                                                                                          |
| Email                | O      | String | Contact email to the carrier                                                                                       | Ex: “transport@tkblueagency.com                                                                |
| LastName             | O      | String | Name of the contact at the carrier                                                                                 | Ex: “DURAND”                                                                                   |
| FirstName            | O      | String | First name of the contact at the carrier                                                                           | Ex: “Pierre”                                                                                   |
| BusinessName         | O      | String | Company name of the carrier                                                                                        | Ex: “Transports Valognes SARL”                                                                 |
| idLanguage           | O      | Int    | Carrier’s language identifier                                                                                      | Ex: 1 for English, 2 for French                                                                |
| CountryCode          | O      | String | ISO Country Code. This code must be specified when it is not present in the first two characters of the VAT number | Ex: “FR” for France                                                                            |
| Acronym TIN          | O      | String | This acronym must be specified when the carrier identifier does not correspond to an intra-Community VAT number    | Ex: “VAT” for a VAT number                                                                     |

**Nature of the fields**

Fields with the nature M (mandatory) are mandatory. The absence of such a field or a nil value will result in the incorrect processing of carrier correspondence.

Those whose nature is O (optional) can be absent or filled in with a null value, without causing processing error.

Fields marked with an asterisk can be filled in using the correspondence settings.

**Information CO2**

The idAdemeCO2 field allows to assign a default CO2 level 1 index to the carrier waiting for its declaration.

Once the carrier has calculated its own CO2 index, it will automatically replace the default assigned index without user action.

**Mode of transport**

To set up a carrier connection that operates on several modes of transport with a specific fleet for each mode, it is then necessary to create several settings for each mode of transport

**Call settings**

If the chosen format is CSV, each correspondence must be separated by an end of line character and must contain the information previously described in the JSON structure, separated by the character defined by csvDelimit.

The different fields must be ordered according to the order of the fields indicated in the table above.

#### Output parameters

The return of the[**putCarrierCorrespondancyList**](/php-reference/#documentation-for-models) function is a string in json format containing

| Parameter name | Description                                                                                   |
| -------------- | --------------------------------------------------------------------------------------------- |
| Imported       | The total number of imported matches (complete or incomplete)                                 |
| Rejected       | The total number of rejected matches                                                          |
| Incoherent     | The total number of inconsistencies encountered (e.g. mode of transport and fleet identifier) |
| Invitations    | The total number of invitations sent automatically by e-mail                                  |
| Errors         | The list of errors encountered in json format.                                                |

#### Example of a code

The import can be tested from a web page containing the following php code as an example:

@[code](./codes/php/envoiDesDonnées.php)

**Example of the content of the variable $\_POST\[‘list’\] with the choice of the JSON format:**

```json
[
  { "Name": "WSTR1", "TIN": "FR62421868084" },
  {
    "Name": "DONAVIN",
    "TIN": "06141912850013",
    "idModality": "Urban Road",
    "Email": "abcd@xyz.fr",
    "FirstName": "Paul",
    "LastName": "DUPONT",
    "BusinessName": "SARL DUPONT",
    "idLanguage": 2,
    "CountryCode": "SV",
    "TINAcr": "NIT"
  }
]
```

**Example of the content of the variable $\_POST\[‘list’\] with the choice of the CSV format:**

```csv
"WSTR1","FR62421868084"
"DONAVIN","06141912850013","Urban Road",,,,"abcd@xyz.fr","Paul","DUPONT","SARL DUPONT",2,"SV","NIT"
```

**Example of the returned string:**

```json
{
  "Imported": 0,
  "Rejected": 3,
  "Incoherent": 0,
  "Invitation": 0,
  "Errors": [
    "line o:InvalidVATNumber",
    "line 1:InvalidVATNumber",
    "line 2:InvalidVATNumber"
  ]
}
```

## Querying the Blue Gallery

The interrogation procedure is carried out in 2 steps:

- Identification with token recovery
- Data recovery

The authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function and providing the member’s email and sha1 encrypted password. This function returns an authentication token

Then call the [**getCarrierList**](/php-reference/#documentation-for-models) function with the email previously provided, the returned authentication token and a string of characters to describe the chosen transfer options.

[**Requires authentication**](#authentication-and-authorization)

### Querying the Blue Gallery

#### Data Recovery

Once the authentication token obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function you must call the [**getCarrierList**](/php-reference/#documentation-for-models) function This function has 3 input parameters: the member’s email, the authentication token and a transfer options field. It returns information about the selected carriers.

##### Input parameters

| Field name           | Type   | Description                              |
| -------------------- | ------ | ---------------------------------------- |
| Email                | String | Member’s login ID on the platform        |
| Authentication token | String | Returned by the ConnectNotation function |
| Transfer options     | String | Detailed below                           |

The “transfer options” field is a string of characters that will be analyzed by a PHP call of the PARSE QUERY type, i.e. it consists of one or more options of the form “param1=val1&param2=val2….. »

**The possible options are described below:**

| Field name   | Type    | Description                                                                                                                                                                                                                                                                                                                        | Value                                                                                                                               |
| ------------ | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| pagenumber   | Int     | Page number in all search results when using a paged search. The selection range is specified by a page number and a number of results (linenumber) per page to be returned.                                                                                                                                                       | The default value is 0                                                                                                              |
| linenumber   | Int     | Maximum number of benefits to be returned. The selection range is specified by a page number (pagenumber) and a number of results per page to be returned. The default value is 100, it is also the maximum possible value. To recover more than 100 services, it is necessary to schedule successive calls and manage the paging. | The default value is 100                                                                                                            |
| myScopeOnly  | Boolean | Determination of the carriers surveyed: all carriers or only the carriers mentioned in its flows.                                                                                                                                                                                                                                  | “False” by default. “True” to restrict to its own carriers                                                                          |
| Modality     | String  | Title of the mode of transport                                                                                                                                                                                                                                                                                                     | “Urban Road” , “Interurban Road” , “Rail” , “River” , “Short Sea Shipping” , “Deep Sea” , “Air”                                     |
| BusinessName | String  | Company name of the carrier                                                                                                                                                                                                                                                                                                        |                                                                                                                                     |
| VatNumber    | String  | Carrier’s NIF                                                                                                                                                                                                                                                                                                                      | Ex “FR62421868084” if the identifier is an intra-Community VAT number, “123456789B\|SG\|UEN” for a UEN type identifier in Singapore |
| GHG          | String  | Choice of GHG display                                                                                                                                                                                                                                                                                                              | FR (default)/ EN-Gp / EN-Gr                                                                                                         |

**Examples:**

- Modality=Rail&VatNumber=FR09123456789,
- Modality=Air&VatNumber=ABC123456UV4|MX|RFC
- Modality=URBAN ROAD&GHG=EN-Gr
- GHG=FR&pagenumber=2&linenumber=10

##### Output parameters

The return of the [**getCarrierList**](/php-reference/#documentation-for-models) function is a string in json format containing 2 parameters:

| Field name | Type | Description                                                                                                                                                                                                                                           |
| ---------- | ---- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Total      | Int  | The total number of carriers concerned by the search criteria excluding pagination                                                                                                                                                                    |
| data       | JSON | A JSON string containing the following information for each selected carrier: BusinessName (company name), VatNumberString (carrier’s NIF), AvgIT (average TK’T index), AvgICO2 (CO2/GES index average depending on the unit chosen by GHG parameter) |

```json
{
  "total": "28",
  "data": [
    {
      "BusinessName": "AGGREGATEUR TEST",
      "VatNumberString": "FR00521890939",
      "AvgIT": 3.2,
      "AvgICO2": 750.4
    },
    ...
  ]
}
```

[**Supports pagination**](#pagination)

##### Example of a code

```php
// determiner les données utilisateurs $email,$password
if (isset($_POST["ETKBAconnect"])) {
    // désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue.wsdl"
    );

    // première étape : obtenir le token

    // executer la méthode connectNotation
    $login = $_POST["LoginEmail"];
    $pwd = hash("sha256", utf8_encode($_POST["Pwd"]));
    try {
        $link = $clientSOAP->connectNotation($login, $pwd);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }

    // deuxième étape : déterminer les critères de recherche et lancer l'interrogation

    // gestion de la pagination sur getCarrierList
    $linenumber = 5;
    $pagenumber = 1;
    $options = $_POST["option"] . "&linenumber=" . $linenumber;
    try {
        $res = $clientSOAP->getCarrierList($login, $link, $options);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }
    $retour = json_decode($res);
    $data = $retour->data;

    if ($retour->total > count($data)) {
        do {
            $pagenumber++;
            $options =
                $_POST["option"] .
                "&linenumber=" .
                $linenumber .
                "&pagenumber=" .
                $pagenumber;
            $res = $clientSOAP->getCarrierList($login, $link, $options);
            $retour = json_decode($res);
            $data = $retour->data;

            // gérer le contenu retourné dans $data
        } while (count($data) == $linenumber);
    }
}

```

The interrogation procedure is carried out in 2 steps: • Identification with token recovery • Data recovery.

The authentication token is obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function and providing the member’s email and sha1 encrypted password. This function returns an authentication token.

Then call the [**getCarrierPerformance**](/php-reference/#documentation-for-models) function with the email previously provided, the returned authentication token and a string of characters to describe the chosen transfer options.

[**Requires authentication**](#authentication-and-authorization)

### Querying a carrier's performances

#### Data Recovery

Once the authentication token obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function you must call the [**getCarrierPerformance **](/php-reference/#documentation-for-models)function This function has 3 input parameters: the member’s email, the authentication token and a transfer options field. It returns information about the selected carrier.

##### Input parameters

| Field name           | Type   | Description                              |
| -------------------- | ------ | ---------------------------------------- |
| Email                | String | Member’s login ID on the platform        |
| Authentication token | String | Returned by the ConnectNotation function |
| Transfer options     | String | Detailed below                           |

The “transfer options” field is a string of characters that will be analyzed by a PHP call of the PARSE QUERY type, i.e. it consists of one or more options of the form “param1=val1&param2=val2….. »

**The possible options are described below:**

| Field name | Type   | Description                                                                                                                   | Value                                                                                                                                                                          |
| ---------- | ------ | ----------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| VatNumber  | String | Carrier’s NIF                                                                                                                 | No default value, this field is mandatory. Ex “FR62421868084” if the identifier is an intra-Community VAT number, “123456789B\|SG\|UEN” for a UEN type identifier in Singapore |
| Modality   | String | Title of the mode of transport among: “Urban Road”, “Interurban Road”, “Rail” “River” “Short Sea Shipping”, “Deep Sea”, “Air” | The default value is “Interurban Road”.                                                                                                                                        |

**Examples:** • Modality=Rail&VatNumber=FR09123456789, • Modality=Air&VatNumber=ABC123456UV4|MX|RFC • VatNumber=FR09123456789

##### Output parameters

The return of the [**getCarrierPerformance**](/php-reference/#documentation-for-models) function is a string in json format containing 3 parameters:

| Field name   | Type   | Description                                                                                                                                                                                                                                                                                      |
| ------------ | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| BusinessName | String | The company name corresponding to the intra-community VAT number if this carrier is registered on the TK’Blue platform                                                                                                                                                                           |
| Accuracy     | String | The accuracy of the information returned: “Default” if default performances are returned, either because the carrier is not yet registered or because he has not yet made a fleet declaration in the desired mode of transport, “Carrier” if the returned performances are those of the carrier. |
| List         | JSON   | A JSON chain containing the “TKT”, “CO2” and “Euro” keys combining the indicators available for each selected fleet. The content of these 3 keys is detailed below                                                                                                                               |

**TKT**

This key groups the characteristics of the TK’Blue fleet:

| Field name           | Type    | Description                                                                               |
| -------------------- | ------- | ----------------------------------------------------------------------------------------- |
| idFlotteTransporteur | Int     | Internal fleet identifier                                                                 |
| FlotteLabel          | String  | Full fleet title (what must be communicated in the Shipper or Transport Organizer flows). |
| DateUpdate           | Date    | Date of creation or last update of the fleet in YYYYY-MM-DD                               |
| Index                | Decimal | Value of the TK’T index of the fleet (between 0 and 100).                                 |
| FlotteCategory       | String  | Fleet category title                                                                      |

**CO2**

This key groups the characteristics of the CO2 index associated by default with the TK’Blue fleet:

| Field name  | Type    | Description                                                                             |
| ----------- | ------- | --------------------------------------------------------------------------------------- |
| IndexFr     | Decimal | CO2 index in gCO2/t.km calculated in accordance with French decree (from well to wheel) |
| IndexGr     | Decimal | GHG index in gCO2e/t.km calculated in accordance with EN 16258 (from tank to wheel).    |
| IndexGp     | Decimal | GHG index in gCO2/t.km calculated in accordance with EN 16258 (from well to wheel)      |
| Level       | Int     | Index level (according to the classification of the French decree).                     |
| IndexRef    | String  | Index reference to be specified in the Shipper or Transport Organizer flows             |
| FlotteLabel | String  | Full title of the index with its reporting date (if level > 1).                         |

**Euro**

This key groups together the characteristics of the valuation in negative external costs of the TK’Blue fleet and its associated CO2 index:

| Field name | Type    | Description                                                                                      |
| ---------- | ------- | ------------------------------------------------------------------------------------------------ |
| Cost       | Decimal | Total cost of negative externalities in c€/t.km                                                  |
| TrustLevel | Int     | Reliability index of the association of the CO2 index with the TK’Blue fleet, between 0 and 100. |

**Example of the returned string:**

```json
{
  "List": [
    {
      "TKT": {
        "idFlotteTransporteur": "1998",
        "FlotteLabel": "OFValIRAvecNiv1-1998-2",
        "DateUpdate": "2017-02-09",
        "Index": 72.59,
        "FlotteCategory": "Road Interurban GVW \u2264 7.5t Gasoil"
      },
      "CO2": {
        "IndexFr": "750.444",
        "IndexGr": "620.889",
        "IndexGp": "774.889",
        "Level": "1",
        "IndexRef": "0|A|21",
        "FlotteLabel": "SuiviAssocN1 (09/02/2017)"
      },
      "Euro": { "Cost": 15.71, "TrustLevel": 75 }
    },
    ...
  ],
  "Accuracy": "Carrier",
  "BusinessName": "AVECSUIVI"
}
```

##### Example of a code

```php
// determiner les données utilisateurs $email,$password
if (isset($_POST["ETKBAconnect"])) {
    // désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue.wsdl"
    );

    // première étape : obtenir le token

    // executer la méthode connectNotation
    $login = $_POST["LoginEmail"];
    $pwd = hash("sha256", utf8_encode($_POST["Pwd"]));
    try {
        $link = $clientSOAP->connectNotation($login, $pwd);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }

    // deuxième étape : déterminer les critères de recherche et lancer l'interrogation
    $options = $_POST["option"];
    try {
        $res = $clientSOAP->getCarrierPerformance($login, $link, $options);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }
    $retour = json_decode($res, true);

    // affichage des résultats
    echo "Accuracy :" .
        $retour["Accuracy"] .
        '
';
    echo "BusinessName :" . $retour["BusinessName"];

    $list = $retour["List"];
    foreach ($list as $l) {
    }
}
```

| TKT                                            |                                                   |                                          |                                               | CO2                                            |                                            |                                             |                                          | TK'€                                     |                                                |
| ---------------------------------------------- | ------------------------------------------------- | ---------------------------------------- | --------------------------------------------- | ---------------------------------------------- | ------------------------------------------ | ------------------------------------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------------- |
| TK'T fleet                                     | Category                                          | TK'T Index                               | Date                                          | CO2 fleet                                      | CO2 index                                  | Ref                                         | Level                                    | Total cost                               | Reliability                                    |
| &lt;? echo $l\['TKT'\]\['FlotteLabel'\]; ?&gt; | &lt;? echo $l\['TKT'\]\['FlotteCategory'\]; ?&gt; | &lt;? echo $l\['TKT'\]\['Index'\]; ?&gt; | &lt;? echo $l\['TKT'\]\['DateUpdate'\]; ?&gt; | &lt;? echo $l\['CO2'\]\['FlotteLabel'\]; ?&gt; | &lt;? echo $l\['CO2'\]\['IndexFr'\]; ?&gt; | &lt;? echo $l\['CO2'\]\['IndexRef'\]; ?&gt; | &lt;? echo $l\['CO2'\]\['Level'\]; ?&gt; | &lt;? echo $l\['Euro'\]\['Cost'\]; ?&gt; | &lt;? echo $l\['Euro'\]\['TrustLevel'\]; ?&gt; |

The interrogation procedure is carried out in 2 steps:

- Identification with token recovery
- Data recovery.

The authentication token is obtained by calling the [**connectNotation**](https://www.tkblueagency.com/en/reference/) function and providing the member’s email and sha1 encrypted password. This function returns an authentication token.

Then call the [**getCarrierStatistics**](/php-reference/#documentation-for-models) function with the email previously provided, the returned authentication token and a string of characters to describe the chosen transfer options.

[**Requires authentication**](#authentication-and-authorization)

### Exporting your carriers' statistics

#### Data Recovery

Once the authentication token obtained by calling the [**connectNotation**](/php-reference/#documentation-for-models) function you must call the [**getCarrierStatistics**](/php-reference/#documentation-for-models) function This function has 3 input parameters: the member’s email, the authentication token and a transfer options field. It returns information about the selected period.

##### Input parameters

| Field name           | Type   | Description                              |
| -------------------- | ------ | ---------------------------------------- |
| Email                | String | Member’s login ID on the platform        |
| Authentication token | String | Returned by the ConnectNotation function |
| Transfer options     | String | Detailed below                           |

The “transfer options” field is a string of characters that will be analyzed by a PHP call of the PARSE QUERY type, i.e. it consists of one or more options of the form “param1=val1&param2=val2….. »

**The possible options are described below:**

| Field name | Type   | Description                                                                                                                                                                                                                                                                                                                       | Value                                                                                                                          |
| ---------- | ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| pagenumber | Int    | Page number in all search results when using a paged search. The selection range is specified by a page number and a number of results (linenumber) per page to be returned.                                                                                                                                                      | The default value is 0                                                                                                         |
| linenumber | Int    | Maximum number of results to be returned. The selection range is specified by a page number (pagenumber) and a number of results per page to be returned. The default value is 100, it is also the maximum possible value. To retrieve more than 100 results, it is necessary to schedule successive calls and manage the paging. | The default value is 100                                                                                                       |
| Year       | Int    | The year of statistics.                                                                                                                                                                                                                                                                                                           | y-2, y-1, y, y, y+1 where y is the current year at the time of the start of the query                                          |
| Month      | Int    | Allows you to retrieve the results for a specific month of a year, or for a whole calendar year, or for a whole fiscal year; The period corresponding to a fiscal year is defined in the shipper area.                                                                                                                            | O (default value) for annual statistics,<br><br>13 for annual statistics on fiscal year,<br><br>1 to 12 for monthly statistics |
| Modality   | String | Title of the mode of transport. This data is mandatory, as results cannot be obtained in all modes.                                                                                                                                                                                                                               | “Urban Road” , “Interurban Road” , “Rail” , “River” , “Short Sea Shipping” , “Deep Sea” , “Air”                                |

**Examples:**

- Modality=Rail&Year=2016,
- Modality=URBAN ROAD&Year=2017&Month=3

##### Output parameters

The return of the **getCarrierStatistics** function is a string in json format containing 2 parameters:

| Field name | Type | Description                                                                                                                                                                                                                                                            |
| ---------- | ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Total      | Int  | The total number of carriers concerned by the search criteria excluding pagination                                                                                                                                                                                     |
| data       | JSON | A JSON string containing the following information for each selected carrier: BusinessName (company name), VatNumberString (intra-community VAT number), AvgIT (average TK’T index), AvgICO2 (CO2/GES index average according to the unit chosen by the GHG parameter) |

Each item of the returned data variable contains the following information:

| Field name      | Type    | Description                                                                                                        |
| --------------- | ------- | ------------------------------------------------------------------------------------------------------------------ |
| BusinessName    | String  | The carrier’s company name                                                                                         |
| VatNumberString | Sring   | Carrier’s NIF                                                                                                      |
| TKM             | Decimal | The total number of tonne-kilometres operated on behalf of the shipper by that carrier                             |
| ITKT            | Decimal | Its average TK’T index observed in flows, ranging from 0 to 100                                                    |
| COST            | Decimal | The total societal cost in €                                                                                       |
| SCO2            | Decimal | the total CO2 emissions according to the French decree (from well to wheel) in kgCO2                               |
| SGp             | Decimal | total GHG emissions according to the European standard (from well to wheel) in kgCO2e                              |
| SGr             | Decimal | the total GHG emissions according to the European standard (from tank to wheel) in kgCO2e                          |
| ICO2            | Decimal | Its average CO2 index observed in the flows, according to the French decree (from well to wheel) in gCO2/t.km      |
| IGp             | Decimal | Its average GHG index observed in the flows, according to the European standard (from well to wheel) in gCO2e/t.km |
| IGp             | Decimal | Its average GHG index observed in the flows, according to the European standard (from tank to wheel) in gCO2e/t.km |

```json
{
  "total": "2",
  "data": [
    {
      "BusinessName": "INTERROUTEUN",
      "VatNumber": "FR02325625440",
      "TKM": "2000000.0000",
      "ITKT": 89.335,
      "COST": "3788.8880",
      "CO2": "131114.0000",
      "Gr": "108480.0000",
      "Gp": "135386.0000",
      "ICO2": 6.56,
      "IGr": 5.42,
      "IGp": 6.77
    },
    ...
  ]
}
```

[**Supports pagination**](#pagination)

##### Example of a code

@[code](./codes/php/récupérationDesDonnées.php)

## Accessing to the TK'Blue platform without authentication

To be able to connect seamlessly to the dedicated TK’Blue Chargers or Transport Organizers spaces, i.e. without manually entering the ID and password, you must request an authentication token.

Authentication tokens have a limited lifetime and become inactive as soon as they have been used, so there is no need to store them for later use. In other words, an authentication token must be requested for each connection.

### Identification

The authentication token is obtained by calling the [**connectWeb**](/php-reference/#documentation-for-models) function. This function has 2 parameters in input: the member’s email and sha2 encrypted password and returns a redirection link containing the authentication token.

#### Input parameters

| Field name | Type   | Description                                           |
| ---------- | ------ | ----------------------------------------------------- |
| Email      | String | Member’s login ID on the platform                     |
| Password   | String | sha2 encryption of the login password on the platform |

#### Output parameters

The function returns a link to use to access the member space directly without going through authentication.

#### Example of a code

@[code](./codes/php/identification.php)
