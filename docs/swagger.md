# tkblueagency

Retourne une phrase simple

More information: [https://helloreverb.com](https://helloreverb.com)

Contact Info: [hello@helloreverb.com](hello@helloreverb.com)

Version: 1.0

BasePath:/ws.html

All rights reserved

http://apache.org/licenses/LICENSE-2.0.html

## Access

## Methods

\[ Jump to [Models](#__Models) \]

### Table of Contents

#### [TkblueagencyBinding](#TkblueagencyBinding)

- [`post /connectAdmin`](#connectAdmin)
- [`post /connectWeb`](#connectWeb)
- [`post /connectWebPartner`](#connectWebPartner)
- [`post /disconnectWeb`](#disconnectWeb)
- [`post /ssoWebPartner`](#ssoWebPartner)

#### [TkblueagencyNotationBinding](#TkblueagencyNotationBinding)

- [`post /connectNotation`](#connectNotation)
- [`post /disconnectNotation`](#disconnectNotation)
- [`post /downloadChargeurDeclaration`](#downloadChargeurDeclaration)
- [`post /getCarrierList`](#getCarrierList)
- [`post /getCarrierPerformance`](#getCarrierPerformance)
- [`post /getCarrierStatistics`](#getCarrierStatistics)
- [`post /getLastErrorList`](#getLastErrorList)
- [`post /getPostponedImportStatus`](#getPostponedImportStatus)
- [`post /putCarrierCorrespondancyList`](#putCarrierCorrespondancyList)
- [`post /sendChargeurDeclaration`](#sendChargeurDeclaration)

# TkblueagencyBinding

[Up](#__Methods)

    post /connectAdmin

connectAdmin (connectAdmin)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [connectAdminRequest](#connectAdminRequest) (required)

Body Parameter —

### Return type

[connectAdminResponse](#connectAdminResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [connectAdminResponse](#connectAdminResponse)

---

[Up](#__Methods)

    post /connectWeb

connectWeb (connectWeb)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [connectWebRequest](#connectWebRequest) (required)

Body Parameter —

### Return type

[connectWebResponse](#connectWebResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [connectWebResponse](#connectWebResponse)

---

[Up](#__Methods)

    post /connectWebPartner

connectWebPartner (connectWebPartner)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [connectWebPartnerRequest](#connectWebPartnerRequest) (required)

Body Parameter —

### Return type

[connectWebPartnerResponse](#connectWebPartnerResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [connectWebPartnerResponse](#connectWebPartnerResponse)

---

[Up](#__Methods)

    post /disconnectWeb

disconnectWeb (disconnectWeb)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [disconnectWebRequest](#disconnectWebRequest) (required)

Body Parameter —

### Return type

[disconnectWebResponse](#disconnectWebResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [disconnectWebResponse](#disconnectWebResponse)

---

[Up](#__Methods)

    post /ssoWebPartner

ssoWebPartner (ssoWebPartner)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [ssoWebPartnerRequest](#ssoWebPartnerRequest) (required)

Body Parameter —

### Return type

[ssoWebPartnerResponse](#ssoWebPartnerResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [ssoWebPartnerResponse](#ssoWebPartnerResponse)

---

# TkblueagencyNotationBinding

[Up](#__Methods)

    post /connectNotation

connectNotation (connectNotation)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [connectNotationRequest](#connectNotationRequest) (required)

Body Parameter —

### Return type

[connectNotationResponse](#connectNotationResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [connectNotationResponse](#connectNotationResponse)

---

[Up](#__Methods)

    post /disconnectNotation

disconnectNotation (disconnectNotation)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [disconnectNotationRequest](#disconnectNotationRequest) (required)

Body Parameter —

### Return type

[disconnectNotationResponse](#disconnectNotationResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [disconnectNotationResponse](#disconnectNotationResponse)

---

[Up](#__Methods)

    post /downloadChargeurDeclaration

downloadChargeurDeclaration (downloadChargeurDeclaration)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [downloadChargeurDeclarationRequest](#downloadChargeurDeclarationRequest) (required)

Body Parameter —

### Return type

[downloadChargeurDeclarationResponse](#downloadChargeurDeclarationResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [downloadChargeurDeclarationResponse](#downloadChargeurDeclarationResponse)

---

[Up](#__Methods)

    post /getCarrierList

getCarrierList (getCarrierList)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [getCarrierListRequest](#getCarrierListRequest) (required)

Body Parameter —

### Return type

[getCarrierListResponse](#getCarrierListResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [getCarrierListResponse](#getCarrierListResponse)

---

[Up](#__Methods)

    post /getCarrierPerformance

getCarrierPerformance (getCarrierPerformance)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [getCarrierPerformanceRequest](#getCarrierPerformanceRequest) (required)

Body Parameter —

### Return type

[getCarrierPerformanceResponse](#getCarrierPerformanceResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [getCarrierPerformanceResponse](#getCarrierPerformanceResponse)

---

[Up](#__Methods)

    post /getCarrierStatistics

getCarrierStatistics (getCarrierStatistics)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [getCarrierStatisticsRequest](#getCarrierStatisticsRequest) (required)

Body Parameter —

### Return type

[getCarrierStatisticsResponse](#getCarrierStatisticsResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [getCarrierStatisticsResponse](#getCarrierStatisticsResponse)

---

[Up](#__Methods)

    post /getLastErrorList

getLastErrorList (getLastErrorList)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [getLastErrorListRequest](#getLastErrorListRequest) (required)

Body Parameter —

### Return type

[getLastErrorListResponse](#getLastErrorListResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [getLastErrorListResponse](#getLastErrorListResponse)

---

[Up](#__Methods)

    post /getPostponedImportStatus

getPostponedImportStatus (getPostponedImportStatus)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [getPostponedImportStatusRequest](#getPostponedImportStatusRequest) (required)

Body Parameter —

### Return type

[getPostponedImportStatusResponse](#getPostponedImportStatusResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [getPostponedImportStatusResponse](#getPostponedImportStatusResponse)

---

[Up](#__Methods)

    post /putCarrierCorrespondancyList

putCarrierCorrespondancyList (putCarrierCorrespondancyList)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [putCarrierCorrespondancyListRequest](#putCarrierCorrespondancyListRequest) (required)

Body Parameter —

### Return type

[putCarrierCorrespondancyListResponse](#putCarrierCorrespondancyListResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [putCarrierCorrespondancyListResponse](#putCarrierCorrespondancyListResponse)

---

[Up](#__Methods)

    post /sendChargeurDeclaration

sendChargeurDeclaration (sendChargeurDeclaration)

### Consumes

This API call consumes the following media types via the Content-Type request header:

- `application/xml`

### Request body

Body [sendChargeurDeclarationRequest](#sendChargeurDeclarationRequest) (required)

Body Parameter —

### Return type

[sendChargeurDeclarationResponse](#sendChargeurDeclarationResponse)

### Example data

Content-Type: application/xml

      aeiou

### Produces

This API call produces the following media types according to the Accept request header; the media type will be conveyed by the Content-Type response header.

- `application/xml`

### Responses

#### 200

Successfully retrieved the response [sendChargeurDeclarationResponse](#sendChargeurDeclarationResponse)

---

## Models

\[ Jump to [Methods](#__Methods) \]

### Table of Contents

1.  [`connectAdminRequest` \- connectAdminRequest](#connectAdminRequest)
2.  [`connectAdminResponse` \- connectAdminResponse](#connectAdminResponse)
3.  [`connectNotationRequest` \- connectNotationRequest](#connectNotationRequest)
4.  [`connectNotationResponse` \- connectNotationResponse](#connectNotationResponse)
5.  [`connectWebPartnerRequest` \- connectWebPartnerRequest](#connectWebPartnerRequest)
6.  [`connectWebPartnerResponse` \- connectWebPartnerResponse](#connectWebPartnerResponse)
7.  [`connectWebRequest` \- connectWebRequest](#connectWebRequest)
8.  [`connectWebResponse` \- connectWebResponse](#connectWebResponse)
9.  [`disconnectNotationRequest` \- disconnectNotationRequest](#disconnectNotationRequest)
10. [`disconnectNotationResponse` \- disconnectNotationResponse](#disconnectNotationResponse)
11. [`disconnectWebRequest` \- disconnectWebRequest](#disconnectWebRequest)
12. [`disconnectWebResponse` \- disconnectWebResponse](#disconnectWebResponse)
13. [`downloadChargeurDeclarationRequest` \- downloadChargeurDeclarationRequest](#downloadChargeurDeclarationRequest)
14. [`downloadChargeurDeclarationResponse` \- downloadChargeurDeclarationResponse](#downloadChargeurDeclarationResponse)
15. [`getCarrierListRequest` \- getCarrierListRequest](#getCarrierListRequest)
16. [`getCarrierListResponse` \- getCarrierListResponse](#getCarrierListResponse)
17. [`getCarrierPerformanceRequest` \- getCarrierPerformanceRequest](#getCarrierPerformanceRequest)
18. [`getCarrierPerformanceResponse` \- getCarrierPerformanceResponse](#getCarrierPerformanceResponse)
19. [`getCarrierStatisticsRequest` \- getCarrierStatisticsRequest](#getCarrierStatisticsRequest)
20. [`getCarrierStatisticsResponse` \- getCarrierStatisticsResponse](#getCarrierStatisticsResponse)
21. [`getLastErrorListRequest` \- getLastErrorListRequest](#getLastErrorListRequest)
22. [`getLastErrorListResponse` \- getLastErrorListResponse](#getLastErrorListResponse)
23. [`getPostponedImportStatusRequest` \- getPostponedImportStatusRequest](#getPostponedImportStatusRequest)
24. [`getPostponedImportStatusResponse` \- getPostponedImportStatusResponse](#getPostponedImportStatusResponse)
25. [`putCarrierCorrespondancyListRequest` \- putCarrierCorrespondancyListRequest](#putCarrierCorrespondancyListRequest)
26. [`putCarrierCorrespondancyListResponse` \- putCarrierCorrespondancyListResponse](#putCarrierCorrespondancyListResponse)
27. [`sendChargeurDeclarationRequest` \- sendChargeurDeclarationRequest](#sendChargeurDeclarationRequest)
28. [`sendChargeurDeclarationResponse` \- sendChargeurDeclarationResponse](#sendChargeurDeclarationResponse)
29. [`ssoWebPartnerRequest` \- ssoWebPartnerRequest](#ssoWebPartnerRequest)
30. [`ssoWebPartnerResponse` \- ssoWebPartnerResponse](#ssoWebPartnerResponse)

### `connectAdminRequest` \- connectAdminRequest [Up](#__Models)

idContact

[String](#string)

token

[String](#string)

idExternal

[String](#string)

### `connectAdminResponse` \- connectAdminResponse [Up](#__Models)

return

[String](#string)

### `connectNotationRequest` \- connectNotationRequest [Up](#__Models)

email

[String](#string)

password

[String](#string)

### `connectNotationResponse` \- connectNotationResponse [Up](#__Models)

return

[String](#string)

### `connectWebPartnerRequest` \- connectWebPartnerRequest [Up](#__Models)

partnername

[String](#string)

password

[String](#string)

email

[String](#string)

### `connectWebPartnerResponse` \- connectWebPartnerResponse [Up](#__Models)

return

[String](#string)

### `connectWebRequest` \- connectWebRequest [Up](#__Models)

email

[String](#string)

password

[String](#string)

### `connectWebResponse` \- connectWebResponse [Up](#__Models)

return

[String](#string)

### `disconnectNotationRequest` \- disconnectNotationRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

### `disconnectNotationResponse` \- disconnectNotationResponse [Up](#__Models)

return

[String](#string)

### `disconnectWebRequest` \- disconnectWebRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

### `disconnectWebResponse` \- disconnectWebResponse [Up](#__Models)

return

[String](#string)

### `downloadChargeurDeclarationRequest` \- downloadChargeurDeclarationRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

options

[String](#string)

### `downloadChargeurDeclarationResponse` \- downloadChargeurDeclarationResponse [Up](#__Models)

return

[String](#string)

### `getCarrierListRequest` \- getCarrierListRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

options

[String](#string)

### `getCarrierListResponse` \- getCarrierListResponse [Up](#__Models)

return

[String](#string)

### `getCarrierPerformanceRequest` \- getCarrierPerformanceRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

options

[String](#string)

### `getCarrierPerformanceResponse` \- getCarrierPerformanceResponse [Up](#__Models)

return

[String](#string)

### `getCarrierStatisticsRequest` \- getCarrierStatisticsRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

options

[String](#string)

### `getCarrierStatisticsResponse` \- getCarrierStatisticsResponse [Up](#__Models)

return

[String](#string)

### `getLastErrorListRequest` \- getLastErrorListRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

### `getLastErrorListResponse` \- getLastErrorListResponse [Up](#__Models)

return

[String](#string)

### `getPostponedImportStatusRequest` \- getPostponedImportStatusRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

idPending

[String](#string)

### `getPostponedImportStatusResponse` \- getPostponedImportStatusResponse [Up](#__Models)

return

[String](#string)

### `putCarrierCorrespondancyListRequest` \- putCarrierCorrespondancyListRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

option

[String](#string)

itemlist

[String](#string)

### `putCarrierCorrespondancyListResponse` \- putCarrierCorrespondancyListResponse [Up](#__Models)

return

[String](#string)

### `sendChargeurDeclarationRequest` \- sendChargeurDeclarationRequest [Up](#__Models)

email

[String](#string)

token

[String](#string)

option

[String](#string)

itemlist

[String](#string)

### `sendChargeurDeclarationResponse` \- sendChargeurDeclarationResponse [Up](#__Models)

return

[String](#string)

### `ssoWebPartnerRequest` \- ssoWebPartnerRequest [Up](#__Models)

partnername

[String](#string)

token

[String](#string)

email

[String](#string)

info

[String](#string)

### `ssoWebPartnerResponse` \- ssoWebPartnerResponse [Up](#__Models)

return

[String](#string)
