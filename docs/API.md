# API

## General principles

In this document, a badge by each resource denotes what base URL to use. The following base URLs are supported:

- <Badge vertical="middle" text="Notation" /> - https://www-notation.tkblueagency.com/
- <Badge vertical="middle" text="Labellisation" /> - https://www-labellisation.tkblueagency.com/
- <Badge vertical="middle" text="GCI" /> - https://www.emissions-calculator.com/

::: tip Important
All the base URLs use the HTTPS protocol. This document contains all the possible API URLs for all TKBlue apps; if it's not listed here, it's not supported by us.
:::

Most endpoints use standard CRUD and REST API principles, using `GET`, `POST`, `DELETE`, and `PUT` for updates. All resources are plural; you'd use `GET /api/users` to list all users and `GET /api/users/1` to list a single user.

You must pass an `accept` header with the value of `application/json` for all API requests: `'Accept' => 'application/json',`.

### Authentication

::: danger STOP
API tokens are secret, so do not share or publicly expose them. If you think one of your API tokens has been compromised, generate a new one.
:::

Some endpoints require authentication. Endpoints that do require authentication will be marked as such in the docs.

#### Getting an API token

To get an API token, you should refer to the user guide of the service you want to authenticate with. Usually, TkBlue applications will allow you to create personal API tokens on the websites of each service, after you have logged in.

Alternatively, you can use the `/api/user/token` endpoint to generate a token valid for 2 hours.

```http:no-line-numbers
GET /api/user/login
```

| Parameter  | Type     | Rules                     | Required           | Default | Description                            |
| :--------- | :------- | :------------------------ | :----------------- | :------ | :------------------------------------- |
| `email`    | `string` | An existing email address | :white_check_mark: | -       | The email address to authenticate with |
| `password` | `string` | -                         | :white_check_mark: | -       | The password for the account           |

Sample response:

```json
{
  "token": "eyJ..."
}
```

#### Authenticating requests

::: tip
If you are getting back a `401` error code, this means your token is not valid or the way you are passing it is incorrect. This is different to a `403`, which is telling you that you are not allowed to do something (but your token may be valid).
:::

##### Via query parameters

To authenticate, you can either pass a `api_token` as a query parameter: e.g. `GET /api/users?api_token={token}`.

##### Via the request payload

You can post the API token as part of the actual request payload, especially when making `POST` requests. The form parameter is `api_token`.

##### Via headers

You can provide the API token as a Bearer token in the Authorization header of the request: `'Authorization' => 'Bearer '.$token,`.

### Response status codes

TkBlue APIs return the following status codes in its API:

| Status Code | Description             | Meaning                                                                             |
| :---------- | :---------------------- | :---------------------------------------------------------------------------------- |
| 200         | `OK`                    | All good                                                                            |
| 201         | `CREATED`               | The resource has been successfully created                                          |
| 202         | `ACCEPTED`              | The request made has been accepted, and is scheduled for processing                 |
| 401         | `UNAUTHENTICATED`       | Your API token, or the way you are passing it, is incorrect                         |
| 403         | `FORBIDDEN`             | You are not allowed to do the action                                                |
| 404         | `NOT FOUND`             | The resource you are looking for does not exist                                     |
| 405         | `METHOD NOT ALLOWED`    | You are not allowed to use a given HTTP method (GET, POST, PUT, DELETE, or others)  |
| 422         | `BAD REQUEST`           | Your request is malformed, data required is missing and/or incorrect                |
| 429         | `TOO MANY REQUESTS`     | You have exceeded the rate-limiter limits (60 queries per minute)                   |
| 500         | `INTERNAL SERVER ERROR` | There was a problem with the server - if you encounter this one, please let us know |

More general information about status codes can be found here: [https://developer.mozilla.org/en-US/docs/Web/HTTP/Status](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

### Paginated responses

Some responses are paginated. You can access additional pages by passing the `page` parameter. Paginated responses include additional information to help you build pagination on the frontend.

By default, 15 results will be returned per page. You can choose to return up to `249` records per page or less by passing the `per_page` parameter.

```json
{
	"current_page": 1,
	"data": [
		...
	],
	"first_page_url": "https://www-labellisation.tkblueagency.com/api/models?page=1",
	"from": 1,
	"next_page_url": "https://www-labellisation.tkblueagency.com/api/models?page=2",
	"path": "https://www-labellisation.tkblueagency.com/api/models",
	"per_page": 15,
	"prev_page_url": null,
	"to": 15
}
```

If there is no more next or previous pages, the values will be `null`.

### Rate limiting

A rate limiter is implemented on all endpoints. If you exceed a given rate-limiter limit, you will get back a [`429` status code](#response-status-codes). Wait a few minutes before trying again; note that some responses will tell you how much time you have to wait in the response headers.

## TKT declaration forms <Badge vertical="middle" text="Labellisation" />

::: tip
All the following endpoints require an API token.
:::

TKT declaration forms allow you to declare fleets and information about those fleets in order to calculate your TK Indicies.

Declaration forms are dynamically built based on previous input, so its important to GET the forms before POSTing them, as well as to re-GET the form each time a parameter changes, as it may change what questions are available and required in the declaration form.

### Get a declaration form questions

```http:no-line-numbers
GET /api/app/tkb/carriers/declarations/tkt/create
```

<!-- prettier-ignore-start -->
| Parameter | Type | Rules | Required | Default | Description |
| :--- | :--- | :--- | :--- | :--- | :--- |
| `step` | `int` | A valid step number, from 0 up to the maximum for the current declaration. When fetching a declaration for the first time using step 0, the response will contain an array of steps that will tell you what are the other available steps in the declaration form based on the parameters you passed. When fetching a declaration for the first time, you should always pass a `step=0` | :white_check_mark: | - | Step of the current declaration. |
| `choice_flotte` | `int` | `0` for owned fleets or `1` for composed fleets | - | - | The declaration type |
| `idModality` | `int` | A valid modality ID | - | - | - |
| `idAreaSpecificity` | `int` | A valid Area Specificity ID | - | - | - |
| `idMeanCategory` | `int` | A valid Mean Category ID | - | - | - |
| `idEnergy` | `int` | A valid Energy ID | - | - | - |
| `idTransportSpecificity` | `int` | A valid Transport Specificity ID | - | - | - |
| `FlotteLabel` | `string` | A unique name for the fleet | - | - | - |
<!-- prettier-ignore-end -->

Depending on the parameters you pass when getting the declaration form, different questions and answer-options for each question will be presented.

When fetching a declaration for the first time using step 0, the response will contain an array of steps that will tell you what are the other available steps in the declaration form based on the parameters you passed.

Sample response:

```json
{
    "title": "Nouvelle Déclaration",
    "questions": [
        {
            "Sort": 1,
            "QuestionLabel": "Type de flotte",
            "name": "choice_flotte",
            "options": [
                {
                    "value": 1,
                    "text": "Flotte propre",
                    "TooltipLabel": "<p>Une flotte propre est une flotte repr&eacute;sentant uniquement les moyens de transport qui vous appartiennent en propre ou sous contrat de location, mais dont vous g&eacute;rez la maintenance.</p>\r\n\r\n<p>Il faut d&eacute;clarer une flotte propre par cat&eacute;gorie de moyen de transport et par &eacute;nergie utilis&eacute;e.</p>\r\n"
                },
                {
                    "value": 2,
                    "text": "Flotte composée",
                    "TooltipLabel": "<p>\r\n\tUne &quot;flotte compos&eacute;e&quot; vous permet de d&eacute;clarer une flotte pouvant combiner une de vos flottes avec celles de transporteurs que vous sous-traitez. Il vous est possible de d&eacute;clarer autant de flottes compos&eacute;es que vous le souhaitez.</p>\r\n"
                }
            ],
            "value": 2,
            "idQuestion": "choice_flotte",
            "Step": 0,
            "QuestionType": "SELECT",
            "AnswerType": null,
            "TooltipLabel": null
        },
        ...
    ],
    "submitLabel": "Suivant",
    "print": {
        "url": "https://dockermiwdev-labellisation.tkblueagency.com/print_tkdecltransporteur-0.html?Version=2&Mode=1&FlotteCategory=0&Flotte=Standard",
        "text": "Imprimer un modèle vierge de déclaration"
    },
    "steps": [
        {
            "value": 1,
            "text": "Catégorie de flotte"
        },
        {
            "value": 2,
            "text": "Composition / Répartition"
        },
        {
            "value": 3,
            "text": "Simulation / Mon indice"
        }
    ]
}
```

#### Note on question types

Each question has a `QuestionType` field. This refers to the type of question/answer that is expected, and can help you decide what kind of input to use.

<!-- prettier-ignore-start -->
| QuestionType | Suggested input type |
| :--- | :--- |
| `INT` | `number` |
| `POS` | `number` |
| `Y/N` | `radio` |
| `ECO` | `select`, but with an additional `text` input that can support custom inputs not found within the select, especially if the select has its first value selected (often, `other`) |
| `HIDDEN` | `hidden` |
| `SEP` | `div`, this is a visual separator, and does not accept any input |
| `SELECT` | `select` |
| `PCT_MULTISELECT` | `text` input for each option in the question, where each input can take 0-100 (as its a percent value) |
| Default and others | `text` input |
<!-- prettier-ignore-end -->

### Post a declaration form

```http:no-line-numbers
POST /api/app/tkb/carriers/declarations/tkt/create
```

Note: a declaration form is only actually created on the second to last step, see: [submitting the declaration form](#submitting-the-form).

<!-- prettier-ignore-start -->
| Parameter | Type | Rules | Required | Default | Description |
| :--- | :--- | :--- | :--- | :--- | :--- |
| `step` | `int` | A valid step number, from 0 up to the maximum for the current declaration. | - | - | Step of the current declaration. |
| `choice_flotte` | `int` | `0` for owned fleets or `1` for composed fleets | - | - | The declaration type |
| `idModality` | `int` | A valid modality ID | - | - | - |
| `idAreaSpecificity` | `int` | A valid Area Specificity ID | - | - | - |
| `idMeanCategory` | `int` | A valid Mean Category ID | - | - | - |
| `idEnergy` | `int` | A valid Energy ID | - | - | - |
| `idTransportSpecificity` | `int` | A valid Transport Specificity ID | - | - | - |
| `FlotteLabel` | `string` | A unique name for the fleet | - | - | - |
| Question data: see: [getting a declaration form](#passing-question-data-parameters) | - | - | - | - | - |
<!-- prettier-ignore-end -->

#### Passing question data parameters

Depending on the parameters you pass as noted in the parameter table, you will additionally need to pass the values of each question (to get questions, see: [getting a declaration form](#get-a-declaration-form-questions)). The `name` of the question is the key to use; for example, if your question `name` is `Q26_1_1_INFO`, you should pass `{Q26_1_1_INFO: "my value"}` when posting a declaration form.

#### Submitting the form

You can post a request with each step to check the validation of your inputs so far, but the form is only submitted when you post with the `step` parameter equal to the last step - 1, as available on the form. This step will create both the declaration and the fleet. The return of posting to this step may or may not include simulation results.

Posting the last step on the form will return additional information, including the calculated indexes within the `svgs` key in the response.

Sample response:

```json
{
  "idDeclaration": 1,
  "svgs": [
    {
      "html": "<div>...</div>"
    },
    {
      "html": "<div>...</div>"
    }
  ],
  "remainingSimulationCount": null,
  "message": "<p>Une fois votre d&eacute;claration enregistr&eacute;e, et &agrave; d&eacute;faut d&#39;association d&#39;indice GES sp&eacute;cifique pour cette flotte TK&#39;Blue, <strong>l&#39;indice GES de niveau 1 du d&eacute;cret 2017-639 </strong>suivant sera automatiquement utilis&eacute; pour le calcul de l&#39;information GES dans les flux de vos clients chargeurs</p>\r\n",
  "requiredDocuments": {
    "title": [
      "vous devez enregistrer définitivement votre déclaration ",
      "l'imprimer, la signer et l'importer au format PDF depuis l'onglet Mes Documents"
    ],
    "documents": [
      "Cette déclaration signée (vous pouvez l'imprimer en cliquant sur le bouton Imprimer ci-contre)<button name=\"btn_print\" id=\"btn_print\" type=\"button\" onclick=\"window.open('/print_tkdecltransporteur-3622.html','_blank')\"  class=\"btn btn-light btn-sm float-right  mx-3\"><span><i class=\"fas fa-print\">&nbsp;</i>Imprimer</span></button>",
      "Votre Etat du parc daté, signé et tamponné mentionnant l'immatriculation et la norme Euro de chaque véhicule",
      "Votre Attestation de fourniture de déclarations sociales datant de moins de 6 mois",
      "Votre Déclaration datée, signée et tamponnée à l'inspection du travail des salariés détachés",
      "Votre Extrait (de moins de 3 mois) de l'inscription au registre du commerce et des sociétés (K'Bis)"
    ]
  },
  "fleetId": 3274,
  "combinationReliability": {
    "raw": {
      "energyUsed": [],
      "energyBlue": ["Électricité (Europe hors France)"],
      "compatibility": true,
      "utm_pb": false,
      "ITtmp": [
        {
          "IT": 0.5963314919911128,
          "ITF": 0,
          "DefUTM": "4.52",
          "DefKm": "0",
          "DefConso": "0.626",
          "DefConso_2": "0.000",
          "DefEmport": "23.00",
          "DefMPConso": "0.345",
          "DefSlopeConso": "0.1021",
          "ExtC_4": "100",
          "ExtI_4": "1.54960",
          "ExtC_5": "100",
          "ExtI_5": "0.68100",
          "ExtC_6": "100",
          "ExtI_6": "1.01000",
          "ConsoP": 0.341034,
          "CO2P": 0,
          "NOxP": 0,
          "SO2P": 0,
          "PM25P": 0,
          "PM10P": "0.000",
          "NMVOCP": 0,
          "HCP": "0.000",
          "ExtV_1": 0.35184,
          "ExtV_2": 8.8885,
          "ExtV_3": 0.24324,
          "ExtV_4": 0.3428318584070797,
          "ExtV_5": 0.15066371681415933,
          "ExtV_6": 0.8636382313991563,
          "TTWNMVOC": 0,
          "TTWNOx": 0,
          "TTWSO2": 0,
          "TTWPM25": 0.3428318584070797,
          "TTWCO2": 0.15066371681415933,
          "WTTCO2": 0.3449774483362832,
          "WTTNMVOC": 0.000595973183247239,
          "WTTNOx": 0.04911558924446443,
          "WTTSO2": 0.2191528270030732,
          "WTTPM25": 0.026345066198459863
        }
      ],
      "ConsoMax": 0.7339849999999999,
      "ConsoMin": 0.302045,
      "ConsoFleetMax": 0.4950152591999999,
      "ConsoFleetMin": 0.3300101728,
      "total_conso": 0,
      "unittmp": null,
      "AssocC02Desc": {
        "idLabelCO2": "1702",
        "idModality": "1",
        "ITLevel": 1,
        "idFlotteTransporteur": "900",
        "idAdemeCO2": "7",
        "idRef": "A",
        "idDeclTr": "1002",
        "IT": "311.379",
        "ITConso": "1027.550000",
        "TxEmpty": "20.00",
        "UTM": "3.30",
        "UTM_TEU": "0.000",
        "Gr": "250.152",
        "Gp": "312.197",
        "Er": "3515.909",
        "Ep": "4333.333",
        "Label": "ADEME_007",
        "Sort": "7",
        "Energy": "0.270",
        "Energy2": "0.055",
        "Emission": "311.0000",
        "Em_Gp": "312.0000",
        "Em_Gr": "250.0000",
        "Em_Ep": "4333.0000",
        "Em_Er": "3516.0000",
        "DateUpdate": "2017-05-21 15:15:20",
        "KmMin": "0",
        "KmMax": "-1",
        "MeanCat": "",
        "Active": "1",
        "strICO2": "0,31",
        "uniCO2": "kgCO<sub>2</sub>e/t.km",
        "Check": 1
      }
    },
    "html": "<div>...</div>"
  },
  "indexGraph": {
    "title": "Indice de niveau 1 associé",
    "html": "<div>...</div>"
  }
}
```

When submitting the last step, each returned key may contain valuable data.

| Key                        | Description                                                                                                   |
| :------------------------- | :------------------------------------------------------------------------------------------------------------ |
| `idDeclaration`            | The ID of the declaration                                                                                     |
| `svgs`                     | An array of TKT Indices in HTML                                                                               |
| `remainingSimulationCount` | The remaining amount of simulations you can run on the second to last step.                                   |
| `message`                  | An explanatory message about the newly submitted declaration                                                  |
| `requiredDocuments`        | A list of required documents that must be submitted to the agency in order to validate the declaration form   |
| `fleetId`                  | The ID of the newly created fleet                                                                             |
| `combinationReliability`   | The data required to render a combinationReliability chart, and the chart itself in HTML under the `html` key |
| `indexGraph`               | The index chart in HTML under the `html` key                                                                  |

### Get an existing declaration form to edit

```http:no-line-numbers
GET /api/app/tkb/carriers/declarations/tkt/edit/{declarationId}
```

When getting a declaration form to edit, the same philosophy as fetching a new declaration applies. See: [getting a declaration form](#get-a-declaration-form-questions).

### Edit a declaration form

```http:no-line-numbers
PUT /api/app/tkb/carriers/declarations/tkt/edit/{declarationId}
```

When editing a declaration form and submitting it, the same philosophy as creating a new declaration applies. See: [posting a declaration form](#post-a-declaration-form).

Note that when editing a declaration form, it is not possible to change the fleet information. Instead, create a new declaration form with the new fleet data.

## TKT fleets <Badge vertical="middle" text="Labellisation" />

::: tip
All the following endpoints require an API token.
:::

### Get all fleets

You can't perform this operation on this endpoint.

### Get a fleet

```http:no-line-numbers
GET /api/app/tkb/carriers/fleets/{FleetId}
```

Sample response:

```json
{
    "companyName": "TK EXPRESS",
    "fleetName": "TK EXPRESS",
    "updatedAt": "2014-04-04",
    "categoryDescription": "Routier Urbain PL ≤ 7.5t Diesel Ambiant ",
    "description": "",
    "lastTktIndexValue": 29.7,
    "indexGraph": {
        "title": "Indice de niveau 1 associé",
        "html": "<div>...</div>"
    },
    "indexes": [
        {
            "idExternality": 1,
            "icon": "bruit",
            "label": "Bruit",
            "value": "6,41 c€/t.km"
        },
        {
            "idExternality": 2,
            "icon": "congestion",
            "label": "Congestion",
            "value": "35,34 c€/t.km"
        },
        ...
    ],
    "combinationReliability": {
        "raw": {
            "energyUsed": [],
            "energyBlue": [
                "Gazole routier + non routier",
                "Diesel",
                "BioDiesel"
            ],
            "compatibility": true,
            "utm_pb": true,
            "ITtmp": [
                {
                    "idLabelTransporteur": "113",
                    "idDeclTr": "118",
                    "idTransporteur": "226",
                    "idModality": "1",
                    "idFlotteTransporteur": "53",
                    "Year": "2013",
                    "IT": "0.29734",
                    "ITF": "0.000",
                    "DateUpdate": "2014-04-04",
                    "Flotte": "",
                    "ExtV_1": "6.414",
                    "ExtV_2": "35.340",
                    "ExtV_3": "1.222",
                    "ExtV_4": "3.555",
                    "ExtV_5": "3.191",
                    "ExtV_6": "1.735",
                    "ExtV_7": "0.000",
                    "ExtF_1": "0.000",
                    "ExtF_2": "0.000",
                    "ConsoP": "0.278",
                    "CO2P": "0.000",
                    "NOxP": "0.455",
                    "SO2P": "0.000",
                    "PM25P": "0.880",
                    "PM10P": "0.000",
                    "NMVOCP": "0.944",
                    "HCP": "0.000",
                    "IndP_4": "0.00000",
                    "IndP_5": "0.00000",
                    "IndP_6": "0.00000",
                    "ExtC_4": "100",
                    "ExtC_5": "100",
                    "ExtC_6": "100",
                    "ExtI_4": "0.86350",
                    "ExtI_5": "0.00000",
                    "ExtI_6": "0.00000",
                    "DefUTM": "0.90",
                    "DefKm": "0",
                    "DefConso": "0.515",
                    "DefConso_2": "0.110",
                    "DefEmport": "0.02",
                    "DefMPConso": "3.191",
                    "DefSlopeConso": "2.1100",
                    "WTTCO2": "0.002",
                    "WTTNOx": "1.440",
                    "WTTSO2": "0.001",
                    "WTTPM25": "0.900",
                    "WTTNMVOC": "0.000",
                    "TTWCO2": "0.174",
                    "TTWNOx": "0.000",
                    "TTWSO2": "3.000",
                    "TTWPM25": "0.570",
                    "TTWNMVOC": "0.007",
                    "CO2R": "0",
                    "NOxR": "0",
                    "SO2R": "0",
                    "PM25R": "0",
                    "PM10R": "0",
                    "NMVOCR": "0",
                    "StartApply": "2014-04-04",
                    "EndApply": "2100-01-01",
                    "StartValid": "0000-00-00",
                    "EndValid": "0000-00-00"
                }
            ],
            "ConsoMax": 1.6221875,
            "ConsoMin": 0,
            "ConsoFleetMax": 0.5415,
            "ConsoFleetMin": 0.361,
            "total_conso": null,
            "unittmp": null,
            "AssocC02Desc": null
        },
        "html": "<div>...</div>"
    }
}
```

### Create a fleet

You can't perform this operation on this endpoint. To create fleets, complete a declaration form. See: [posting a declaration form](#post-a-declaration-form).

### Get a fleet GHG associations

```http:no-line-numbers
GET /api/app/tkb/carriers/fleets/{FleetId}/ghg/associations
```

Sample response:

```json
[
  {
    "idTransporteur": 105,
    "idModality": 1,
    "ITLevel": 3,
    "idFlotteTransporteur": 1396,
    "idAdemeCO2": 1001,
    "idRef": "G",
    "FlotteLabel": "TestBlendHVO",
    "AdemeCO2Label": "EU+SA-Rigid truck 3,5 - 7,5 t GVW-Average mixed--GOR",
    "IT": 41,
    "idDeclTr": 1573,
    "Full": {
      "idLabelTransporteur": "61362",
      "idCompo": "0",
      "idDeclTr": "1573",
      "idTransporteur": "105",
      "idChargeur": "0",
      "idModality": "1",
      "idFlotteTransporteur": "1396",
      "Year": "2021",
      "IT": "41.233",
      "ITLevel": "3",
      "idRef": "G",
      "idAdemeCO2": "1001",
      "DateUpdate": "2021-08-16",
      "StartApply": "2018-08-16",
      "EndApply": "2024-08-16",
      "ITConso": "197.917391",
      "TxEmpty": "20.00",
      "UTM": "4.800",
      "Ep": "1120.050",
      "Er": "715.824",
      "Gp": "41.359",
      "Gr": "28.582",
      "UTM_Volume": "0.000",
      "UTM_Area": "0.000",
      "UTM_Size": "0.000",
      "UTM_Parcel": "0.000",
      "UTM_Pallet": "0.000",
      "UTM_TEU": "0.000",
      "SKM": "100.000",
      "BlendPCT": "50.0",
      "AVGAS": "0.000000",
      "B30": "0.000000",
      "B100": "0.000000",
      "HVO100": "10.000000",
      "BMC100": "0.000000",
      "BioLNG100": "0.000000",
      "ELEC": "0.000000",
      "ELECEURO": "0.000000",
      "GAZOIL": "0.000000",
      "GNL": "0.000000",
      "GNLM": "0.000000",
      "GNV": "0.000000",
      "GOFE": "0.000000",
      "GOFL": "0.000000",
      "GONR": "0.000000",
      "GOR": "0.000000",
      "GPL": "0.000000",
      "E10": "0.000000",
      "HFO": "0.000000",
      "JETA": "0.000000",
      "JETB": "0.000000",
      "MDO": "0.000000",
      "MGO": "0.000000"
    },
    "associations": [
      {
        "idLabelCO2Link": 1100,
        "idFlotteBlue": 3273,
        "idFlotteCO2": 1396,
        "idRef": "G",
        "idAdemeCO2": 1001,
        "Comment": "",
        "ByDefault": false
      }
    ]
  }
]
```

If the `associations` key does not contain an empty array, it means that the fleet is associated with the current fleet. If it is empty, it means that the fleet listed can be associated with the current fleet, but is not.

### Create a fleet GHG association

```http:no-line-numbers
POST /api/app/tkb/carriers/fleets/{FleetId}/ghg/associations
```

| Parameter | Type     | Rules               | Required           | Default                    | Description                                              |
| :-------- | :------- | :------------------ | :----------------- | :------------------------- | :------------------------------------------------------- |
| `fleetId` | `int`    | -                   | :white_check_mark: | An ID of an existing fleet | The ID of the fleet you are linking the current fleet to |
| `comment` | `string` | Up to 50 characters | -                  | -                          | An optional comment about the link                       |

The response generated by a successful creation will return the newly created linkId.

Sample response:

```json
{
  "linkId": 1077
}
```

## Transport operation computations

This API serves request of computation of GHG emission, for a single transport operation.
Before calling this API, you'll need to get a fresh token, and get references lists needed to pass right parameters to computation.

All endpoints require [authentication](#authentication).

### Allocation type

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/allocation
```

#### Parameters

| Key          | Required | Comment                                                                      |
| ------------ | -------- | ---------------------------------------------------------------------------- |
| languageCode |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |

#### Response example

```json
[
    {
        "id": 0,
        "label": "Weight",
        "language": 1
    },
    {
        "id": 1,
        "label": "Parcel",
        "language": 1
    },
    ...
]
```

### FTL

The API to call to get the list is

```http:no-line-numbers
GET api/app/common/transport/operation/ftl
```

#### Parameters

| Key          | Required | Comment                                                                      |
| ------------ | -------- | ---------------------------------------------------------------------------- |
| languageCode |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |

#### Response example

```json
[
  {
    "label": "LTL",
    "language": 1
  },
  {
    "label": "FTL",
    "language": 1
  }
]
```

### Modality

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/modality
```

#### Parameters

| Key          | Required | Comment                                                                      |
| ------------ | -------- | ---------------------------------------------------------------------------- |
| languageCode |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |

#### Response example

```json
[
    {
        "id": 1,
        "label": "Urban Road",
        "language": 1
    },
    {
        "id": 2,
        "label": "Interurban Road",
        "language": 1
    },
    ...
]
```

### Energy

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/energy
```

#### Parameters

| Key          | Required | Comment                                                                      |
| ------------ | -------- | ---------------------------------------------------------------------------- |
| languageCode |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |

#### Response example

```json
[
    {
        "label": "Diesel (l) Europe",
        "language": 1
    },
    {
        "label": "Diesel (kg) Europe",
        "language": 1
    },
    ...
]
```

### Main modality

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/mainmodality
```

#### Parameters

| Key          | Required | Comment                                                                      |
| ------------ | -------- | ---------------------------------------------------------------------------- |
| languageCode |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |

#### Response example

```json
[
    {
        "id": 1,
        "label": "Road",
        "language": 1
    },
    {
        "id": 3,
        "label": "Rail",
        "language": 1
    },
    ...
]
```

### Vehicle

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/vehicle
```

#### Parameters

| Key            | Required | Comment                                                                      |
| -------------- | -------- | ---------------------------------------------------------------------------- |
| languageCode   |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |
| idMainModality |          | Id main modality returned in [mainmodality API response](#main-modality)                       |

#### Response example

```json
[
    {
        "label": "Porteur 12t-14t",
        "mainmodality_id": 1,
        "mainmodality_label": "Routier",
        "language": 2
    },
    {
        "label": "Porteur 14t-16t",
        "mainmodality_id": 1,
        "mainmodality_label": "Routier",
        "language": 2
    },
    ...
]
```

### Temperature Control Mode

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/temperaturecontrolmode
```

#### Parameters

| Key          | Required | Comment                                                                      |
| ------------ | -------- | ---------------------------------------------------------------------------- |
| languageCode |          | Language code (1=english,2=french,3=spanish,4=german,5=italian,6=portuguese) |

#### Response example

```json
[
    {
        "label": "Ambient",
        "language": 1
    },
    {
        "label": "Reefer",
        "language": 1
    },
    ...
]
```

### GHG Indexes

The API to call to get the list is

```http:no-line-numbers
GET /api/app/common/transport/operation/ghgindices
```

#### Parameters

| Key       | Required           | Comment                                |
| --------- | ------------------ | -------------------------------------- |
| reference | :white_check_mark: | Reference (A=ADEME,G=GLEC,S=SmartWay)  |
| modality  |                    | id modality from [modality API response](#modality) |

#### Response example

```json
[
    {
        "emission": 1945,
        "modality_id": 1,
        "code": "0|A|1",
        "modality": "Routier Urbain",
        "label": "VUL 3,5T PTAC - Express (plis, courses)"
    },
    {
        "emission": 1099,
        "modality_id": 1,
        "code": "0|A|2",
        "modality": "Routier Urbain",
        "label": "VUL 3,5T PTAC - Express (colis)"
    },
    ...
]
```

### Transport Operation Computation

The API to call to get a flow item computation is

```http:no-line-numbers
POST /api/app/common/transport/operation/computation/
```

The posted data are encapsuled in a JSON object.

#### Parameters

Please find below the JSON object description. All keys are required but some of them can be empty.

<!-- prettier-ignore-start -->
| Key  | Can be empty  | Comment  |
| ------------ | ------------ | ------------ |
| allocationType  |   | Valid label for allocation type retrieved with [allocation API](#allocation-type)  |
| ftlOrLtl  |   | Valid label retrieved with [ftl API](#ftl)  |
| temperatureControlMode  |   | Valid label retrieved with [temperaturecontrolmode API](#temperature-control-mode)  |
| energy  | :white_check_mark:  | Valid label retrieved with [energy API](#energy)  |
| energy2  | :white_check_mark:  | Valid label retrieved with [energy API](#energy)  |
| vehicle  | :white_check_mark:  | Valid label retrieved with [vehicle API](#vehicle)  |
| modality  |   | Valid label retrieved with [modality API](#modality) or [main modality API](#main-modality)  |
| TINCarrier  | :white_check_mark:  | TIN identifier for the carrier  |
| TINAcronym  | :white_check_mark:  | Acronym for TIN (NIF/RUT/RUC/...)  |
| TINCountryCode  | :white_check_mark:  | ISO 2-Digit Alpha Country Code  |
| carrierFleetLabel  | :white_check_mark:  | if carrierFleetType=Fleet or empty, then carrierFleetLabel is a composed string of OF (own fleet) or MF (mixed fleet), followed directly by fleetName-idFleet-idModality. In this case, TINCarrier is required.   |
| carrierFleetType  | :white_check_mark:  | Valid values : IMO\|Plate\|Fleet  |
| dateTransport  |   | Date (format YYYY-MM-DD)  |
| distance  |   | Km, numeric positive value  |
| weight  |   | Weight carried, numeric positive value  |
| TEU  |   | Twenty Equivalent Foot (TEU) container carried, numeric positive value  |
| origin  |   | Starting point (VARCHAR 100)  |
| destination  |   | End point (VARCHAR 100)  |
| consumption  | :white_check_mark:  | Required for level-4 on the fly emission calculation (The unit must be the same as indicated in the Energy category choice)|
| consumption2  | :white_check_mark:  | Required for level-4 on the fly emission calculation if second energy used (The unit must be the same as indicated in the Energy category choice)  |
| loadFillRate  | :white_check_mark:  | for the selected allocation key, for on the fly emission calculation (level-4 probably). Percentage must be an number between 0 and 100.  |
| payloadCapacity  | :white_check_mark:  | Vehicle capacity according to allocation key, for on the fly emission calculation (level-4 probably)  |
| emptyDistance  | :white_check_mark:  | empty distance in Km  |
| recommendedGHGDefaultValueCode  |  | Index to use, formatted as : x\|y\|z where x=0, y=reference letter (A=ADEME,G=GLEC,S=SmartWay,I=ICAO,D=DGAC,C=Clean Cargo,E=specific), z=index identifier  |
| computationMode  |   | possible values default\|ModellingConsumption for ModellingConsumption, these params are required and not empty: energy, vehicle, loadFillRate. If filled, emptyDistance will be used to model consumption (if not, 0 will be used)  |
<!-- prettier-ignore-end -->

If no vehicle is specified, nor any other information allowing to deduce a type of vehicle to use, then the default vehicles used by modality are :
 * 1 => PL<7.5 / GVW ≤ 7.5t
 * 2 => PL de 16 à 32T / HGV from 7,5 to 16t
 * 3 => Locomotive / Locomotive
 * 4 => Bateau < 400 t / Boat < 400 t
 * 5 => Navire RoRo / RoRo
 * 6 => PC - 1 200 evp / Container - 1 200 TEU
 * 7 => Moyen Courrier Mixte 0 à 100 sièges / Mixed Medium Haul 50 to 100 seats

#### Response JSON

##### data section
<!-- prettier-ignore-start -->
| Key  | Can be empty  | Comment  |
| ------------ | ------------ | ------------ |
| TEU  |   | Twenty Equivalent Foot (TEU) container carried, numeric positive value  |
| weight  |   | Numeric positive value  |
| distance  |   | Numeric positive value, in Km  |
| EN16258_Level  |   |   |
| X43-072_Level  |   |  Accuracy level (AFNOR Spec X43072) |
| GHG_FR_WTW  |   | WTW GHG FR  |
| GHG_EN_WTW  |   | WTW GHG EN  |
| GHG_EN_TTW  |   | TTW GHG  |
| costWTTCO2  |   | Cost for WTT CO2  |
| costWTTNOx  |   | Cost for WTT NOx  |
| costWTTSO2  |   | Cost for WTT SO2  |
| costWTTPM25  |   | Cost for WTT PM25  |
| costWTTNMVOC  |   | Cost for WTT MVOC  |
| costTTWCO2  |   | Cost for TTW CO2  |
| costTTWNOx  |   | Cost for TTW NOx  |
| costTTWSO2  |   | Cost for TTW SO2  |
| costTTWPM25  |   | Cost for TTW PM25  |
| costTTWNMVOC  |   | Cost for TTW MVOC  |
| costExternality1  |   | Cost for Noise (float)  |
| costExternality2  |   | Cost for Congestion (float)  |
| costExternality3  |   | Cost for Accidents (float)  |
| costExternality4  |   | Cost for Pollution (float)  |
| costExternality5  |   | Cost for Climate change (float)  |
| costExternality6  |   | Cost for Upstream-downstream (float)  |
| allocationType  |   | Valid label for allocation type (see [allocation API](#allocation))  |
| loadFactorAccuracy  |   | Load Factor accuracy (from 1 to 3)  |
| loadFactorAccuracy  |   | Load Factor accuracy (from 1 to 3)  |
| emptyFactorAccuracy  |   | Empty Factor accuracy (from 1 to 3)  |
| consumptionFactorAccuracy  |   | Consumption Factor accuracy (from 1 to 3)  |
<!-- prettier-ignore-end -->

##### info section
| Key  | Can be empty  | Comment  |
| computationMode  |   | Computation mode used (default\|ModellingConsumption)  |
| computedDistance  |   | Distance was computed or not  |
| addCredits  |   | Additionnal credits spent (if computedDistance is true)  |

##### Success response example

```json
{
    "data": {
        "TEU": "0.000",
        "weight": "7000.00",
        "distance": "591.70",
        "EN16258_Level": "3",
        "X43-072_Level": "2.85",
        "GHG_FR_WTW": "996120.2538",
        "GHG_EN_WTW": "999267.5915",
        "GHG_EN_TTW": "800677.8628",
        "costWTTGHG": "1787.339327",
        "costWTTNOx": "670.804909",
        "costWTTSO2": "1162.167788",
        "costWTTPM25": "248.092854",
        "costWTTNMVOC": "50.598680",
        "costTTWGHG": "7206.100023",
        "costTTWNOx": "8886.814689",
        "costTTWSO2": "5.392760",
        "costTTWPM25": "876.027697",
        "costTTWNMVOC": "59.583734",
        "costExternality1": "405.860220",
        "costExternality2": "4170.861412",
        "costExternality3": "949.885622",
        "costExternality4": "9934.706919",
        "costExternality5": "7206.100023",
        "costExternality6": "3919.003557",
        "allocationType": "TEU",
        "loadFactorAccuracy": 3,
        "emptyFactorAccuracy": 3,
        "consumptionFactorAccuracy": 1
    },
    "info": {
        "computationMode": "default",
        "computedDistance": true,
        "addCredits": 1
    }
}
```

##### Errors in input JSON examples

```json
{
  "error": [
    {
      "errorType": "Modalité inconnue",
      "errorValue": "Unkown Modality"
    },
    {
      "errorType": "Distance non numérique",
      "errorValue": "string for a distance"
    },
    {
      "errorType": "Poids non numérique",
      "errorValue": "put a weight"
    },
    {
      "errorType": "Format de date incorrect",
      "errorValue": "20200901"
    },
    {
      "errorType":"Transporteur inconnu",
      "errorValue":"HR85106651596"
    },
    {
      "errorType":"Flotte sans transporteur",
      "errorValue":0
    }
  ],
    "info": {
        "computationMode": "default",
        "computedDistance": false,
        "addCredits": 0
    }
}
```

##### Errors in computation examples

No data to compute :

```json
{
  "error": "no data to compute"
}
```

Shipper is locked :

```json
{
  "error": "lock"
}
```
