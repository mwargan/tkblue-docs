import { defineUserConfig } from 'vuepress'
import type { DefaultThemeOptions } from 'vuepress'
import { path } from '@vuepress/utils'

export default defineUserConfig<DefaultThemeOptions>({
    lang: 'en-US',
    title: 'TKBlueAgency Docs',
    description: 'Just playing around',

    themeConfig: {
        logo: 'https://www.tkblueagency.com/wp-content/uploads/2018/09/logo_JKPM.png',
        docsRepo: 'https://gitlab.com/mwargan/tkblue-docs',
        docsDir: 'docs',
        displayAllHeaders: true,
        navbar: [

            {
                text: 'API',
                children: [
                    {
                        text: 'Labellisation',
                        children: [
                            '/API',
                        ],
                    },
                ],
            },

            {
                text: 'SOAP',
                children: [
                    {
                        text: 'SOAP general guide',
                        children: [
                            '/',
                        ],
                    },
                    {
                        text: 'PHP Swagger Reference',
                        children: ['/php-reference'],
                    },
                ],
            },


        ]

    },
    locales: {
        // The key is the path for the locale to be nested under.
        // As a special case, the default locale can use '/' as its path.
        '/': {
            lang: 'en-US' // this will be set as the lang attribute on <html>

        },
        '/fr/': {
            lang: 'fr-FR'
        }
    },
    plugins: [
        'check-md',
        [
            '@vuepress/plugin-search',
            {
                locales: {
                    '/': {
                        placeholder: 'Search',
                    },
                    '/fr/': {
                        placeholder: 'Chercher',
                    },
                },
            },
        ],
        [
            '@vuepress/register-components',
            {
                componentsDir: path.resolve(__dirname, './components'),
            },
        ],
        [
            '@vuepress/container',
            {
                type: 'authorization',
                locales: {
                    '/': {
                        defaultInfo: 'Authorization required',
                    },
                    '/fr/': {
                        defaultInfo: 'Autorisation requise',
                    },
                },
                before: (info: string): string =>
                    `<div class="custom-container tip authorization">${info ? `<p class="custom-container-title">${info}</p>` : ''}`,
                after: (): string => '<p><small>All authorization requests require you to first be <router-link to="#authentication">authenticated</router-link>.</small></p></div>\n'
            },
        ],
    ],
})
