# Swagger\Client\TkblueagencyNotationBindingApi

All URIs are relative to *https://sandbox-notation.tkblueagency.com/ws.html*

Method | HTTP request | Description
------------- | ------------- | -------------
[**connectNotation**](TkblueagencyNotationBindingApi.md#connectNotation) | **POST** /connectNotation | connectNotation
[**disconnectNotation**](TkblueagencyNotationBindingApi.md#disconnectNotation) | **POST** /disconnectNotation | disconnectNotation
[**downloadChargeurDeclaration**](TkblueagencyNotationBindingApi.md#downloadChargeurDeclaration) | **POST** /downloadChargeurDeclaration | downloadChargeurDeclaration
[**getCarrierList**](TkblueagencyNotationBindingApi.md#getCarrierList) | **POST** /getCarrierList | getCarrierList
[**getCarrierPerformance**](TkblueagencyNotationBindingApi.md#getCarrierPerformance) | **POST** /getCarrierPerformance | getCarrierPerformance
[**getCarrierStatistics**](TkblueagencyNotationBindingApi.md#getCarrierStatistics) | **POST** /getCarrierStatistics | getCarrierStatistics
[**getLastErrorList**](TkblueagencyNotationBindingApi.md#getLastErrorList) | **POST** /getLastErrorList | getLastErrorList
[**getPostponedImportStatus**](TkblueagencyNotationBindingApi.md#getPostponedImportStatus) | **POST** /getPostponedImportStatus | getPostponedImportStatus
[**putCarrierCorrespondancyList**](TkblueagencyNotationBindingApi.md#putCarrierCorrespondancyList) | **POST** /putCarrierCorrespondancyList | putCarrierCorrespondancyList
[**sendChargeurDeclaration**](TkblueagencyNotationBindingApi.md#sendChargeurDeclaration) | **POST** /sendChargeurDeclaration | sendChargeurDeclaration


# **connectNotation**
> \Swagger\Client\Model\ConnectNotationResponse connectNotation($body)

connectNotation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\ConnectNotationRequest(); // \Swagger\Client\Model\ConnectNotationRequest | 

try {
    $result = $apiInstance->connectNotation($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->connectNotation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ConnectNotationRequest**](../Model/ConnectNotationRequest.md)|  |

### Return type

[**\Swagger\Client\Model\ConnectNotationResponse**](../Model/ConnectNotationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **disconnectNotation**
> \Swagger\Client\Model\DisconnectNotationResponse disconnectNotation($body)

disconnectNotation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\DisconnectNotationRequest(); // \Swagger\Client\Model\DisconnectNotationRequest | 

try {
    $result = $apiInstance->disconnectNotation($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->disconnectNotation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\DisconnectNotationRequest**](../Model/DisconnectNotationRequest.md)|  |

### Return type

[**\Swagger\Client\Model\DisconnectNotationResponse**](../Model/DisconnectNotationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadChargeurDeclaration**
> \Swagger\Client\Model\DownloadChargeurDeclarationResponse downloadChargeurDeclaration($body)

downloadChargeurDeclaration

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\DownloadChargeurDeclarationRequest(); // \Swagger\Client\Model\DownloadChargeurDeclarationRequest | 

try {
    $result = $apiInstance->downloadChargeurDeclaration($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->downloadChargeurDeclaration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\DownloadChargeurDeclarationRequest**](../Model/DownloadChargeurDeclarationRequest.md)|  |

### Return type

[**\Swagger\Client\Model\DownloadChargeurDeclarationResponse**](../Model/DownloadChargeurDeclarationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCarrierList**
> \Swagger\Client\Model\GetCarrierListResponse getCarrierList($body)

getCarrierList

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\GetCarrierListRequest(); // \Swagger\Client\Model\GetCarrierListRequest | 

try {
    $result = $apiInstance->getCarrierList($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->getCarrierList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\GetCarrierListRequest**](../Model/GetCarrierListRequest.md)|  |

### Return type

[**\Swagger\Client\Model\GetCarrierListResponse**](../Model/GetCarrierListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCarrierPerformance**
> \Swagger\Client\Model\GetCarrierPerformanceResponse getCarrierPerformance($body)

getCarrierPerformance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\GetCarrierPerformanceRequest(); // \Swagger\Client\Model\GetCarrierPerformanceRequest | 

try {
    $result = $apiInstance->getCarrierPerformance($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->getCarrierPerformance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\GetCarrierPerformanceRequest**](../Model/GetCarrierPerformanceRequest.md)|  |

### Return type

[**\Swagger\Client\Model\GetCarrierPerformanceResponse**](../Model/GetCarrierPerformanceResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCarrierStatistics**
> \Swagger\Client\Model\GetCarrierStatisticsResponse getCarrierStatistics($body)

getCarrierStatistics

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\GetCarrierStatisticsRequest(); // \Swagger\Client\Model\GetCarrierStatisticsRequest | 

try {
    $result = $apiInstance->getCarrierStatistics($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->getCarrierStatistics: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\GetCarrierStatisticsRequest**](../Model/GetCarrierStatisticsRequest.md)|  |

### Return type

[**\Swagger\Client\Model\GetCarrierStatisticsResponse**](../Model/GetCarrierStatisticsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLastErrorList**
> \Swagger\Client\Model\GetLastErrorListResponse getLastErrorList($body)

getLastErrorList

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\GetLastErrorListRequest(); // \Swagger\Client\Model\GetLastErrorListRequest | 

try {
    $result = $apiInstance->getLastErrorList($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->getLastErrorList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\GetLastErrorListRequest**](../Model/GetLastErrorListRequest.md)|  |

### Return type

[**\Swagger\Client\Model\GetLastErrorListResponse**](../Model/GetLastErrorListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPostponedImportStatus**
> \Swagger\Client\Model\GetPostponedImportStatusResponse getPostponedImportStatus($body)

getPostponedImportStatus

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\GetPostponedImportStatusRequest(); // \Swagger\Client\Model\GetPostponedImportStatusRequest | 

try {
    $result = $apiInstance->getPostponedImportStatus($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->getPostponedImportStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\GetPostponedImportStatusRequest**](../Model/GetPostponedImportStatusRequest.md)|  |

### Return type

[**\Swagger\Client\Model\GetPostponedImportStatusResponse**](../Model/GetPostponedImportStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **putCarrierCorrespondancyList**
> \Swagger\Client\Model\PutCarrierCorrespondancyListResponse putCarrierCorrespondancyList($body)

putCarrierCorrespondancyList

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\PutCarrierCorrespondancyListRequest(); // \Swagger\Client\Model\PutCarrierCorrespondancyListRequest | 

try {
    $result = $apiInstance->putCarrierCorrespondancyList($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->putCarrierCorrespondancyList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PutCarrierCorrespondancyListRequest**](../Model/PutCarrierCorrespondancyListRequest.md)|  |

### Return type

[**\Swagger\Client\Model\PutCarrierCorrespondancyListResponse**](../Model/PutCarrierCorrespondancyListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendChargeurDeclaration**
> \Swagger\Client\Model\SendChargeurDeclarationResponse sendChargeurDeclaration($body)

sendChargeurDeclaration

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyNotationBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\SendChargeurDeclarationRequest(); // \Swagger\Client\Model\SendChargeurDeclarationRequest | 

try {
    $result = $apiInstance->sendChargeurDeclaration($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyNotationBindingApi->sendChargeurDeclaration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\SendChargeurDeclarationRequest**](../Model/SendChargeurDeclarationRequest.md)|  |

### Return type

[**\Swagger\Client\Model\SendChargeurDeclarationResponse**](../Model/SendChargeurDeclarationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

