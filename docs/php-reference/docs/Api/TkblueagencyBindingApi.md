# Swagger\Client\TkblueagencyBindingApi

All URIs are relative to *https://sandbox-notation.tkblueagency.com/ws.html*

Method | HTTP request | Description
------------- | ------------- | -------------
[**connectAdmin**](TkblueagencyBindingApi.md#connectAdmin) | **POST** /connectAdmin | connectAdmin
[**connectWeb**](TkblueagencyBindingApi.md#connectWeb) | **POST** /connectWeb | connectWeb
[**connectWebPartner**](TkblueagencyBindingApi.md#connectWebPartner) | **POST** /connectWebPartner | connectWebPartner
[**disconnectWeb**](TkblueagencyBindingApi.md#disconnectWeb) | **POST** /disconnectWeb | disconnectWeb
[**ssoWebPartner**](TkblueagencyBindingApi.md#ssoWebPartner) | **POST** /ssoWebPartner | ssoWebPartner


# **connectAdmin**
> \Swagger\Client\Model\ConnectAdminResponse connectAdmin($body)

connectAdmin

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\ConnectAdminRequest(); // \Swagger\Client\Model\ConnectAdminRequest | 

try {
    $result = $apiInstance->connectAdmin($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyBindingApi->connectAdmin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ConnectAdminRequest**](../Model/ConnectAdminRequest.md)|  |

### Return type

[**\Swagger\Client\Model\ConnectAdminResponse**](../Model/ConnectAdminResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **connectWeb**
> \Swagger\Client\Model\ConnectWebResponse connectWeb($body)

connectWeb

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\ConnectWebRequest(); // \Swagger\Client\Model\ConnectWebRequest | 

try {
    $result = $apiInstance->connectWeb($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyBindingApi->connectWeb: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ConnectWebRequest**](../Model/ConnectWebRequest.md)|  |

### Return type

[**\Swagger\Client\Model\ConnectWebResponse**](../Model/ConnectWebResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **connectWebPartner**
> \Swagger\Client\Model\ConnectWebPartnerResponse connectWebPartner($body)

connectWebPartner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\ConnectWebPartnerRequest(); // \Swagger\Client\Model\ConnectWebPartnerRequest | 

try {
    $result = $apiInstance->connectWebPartner($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyBindingApi->connectWebPartner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ConnectWebPartnerRequest**](../Model/ConnectWebPartnerRequest.md)|  |

### Return type

[**\Swagger\Client\Model\ConnectWebPartnerResponse**](../Model/ConnectWebPartnerResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **disconnectWeb**
> \Swagger\Client\Model\DisconnectWebResponse disconnectWeb($body)

disconnectWeb

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\DisconnectWebRequest(); // \Swagger\Client\Model\DisconnectWebRequest | 

try {
    $result = $apiInstance->disconnectWeb($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyBindingApi->disconnectWeb: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\DisconnectWebRequest**](../Model/DisconnectWebRequest.md)|  |

### Return type

[**\Swagger\Client\Model\DisconnectWebResponse**](../Model/DisconnectWebResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ssoWebPartner**
> \Swagger\Client\Model\SsoWebPartnerResponse ssoWebPartner($body)

ssoWebPartner

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\TkblueagencyBindingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\SsoWebPartnerRequest(); // \Swagger\Client\Model\SsoWebPartnerRequest | 

try {
    $result = $apiInstance->ssoWebPartner($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TkblueagencyBindingApi->ssoWebPartner: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\SsoWebPartnerRequest**](../Model/SsoWebPartnerRequest.md)|  |

### Return type

[**\Swagger\Client\Model\SsoWebPartnerResponse**](../Model/SsoWebPartnerResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

