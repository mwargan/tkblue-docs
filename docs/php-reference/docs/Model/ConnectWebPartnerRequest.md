# ConnectWebPartnerRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partnername** | **string** |  | 
**password** | **string** |  | 
**email** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


