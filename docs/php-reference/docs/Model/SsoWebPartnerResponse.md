# SsoWebPartnerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**return** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


