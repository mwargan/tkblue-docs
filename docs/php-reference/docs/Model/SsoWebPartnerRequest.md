# SsoWebPartnerRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partnername** | **string** |  | 
**token** | **string** |  | 
**email** | **string** |  | 
**info** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


