# ConnectAdminRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_contact** | **string** |  | 
**token** | **string** |  | 
**id_external** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


