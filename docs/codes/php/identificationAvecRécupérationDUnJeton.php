<?php
// lier le client au fichier WSDL
$clientSOAP = new SoapClient(
    "https://sandbox-notation.tkblueagency.com/res/tkblue_sandbox.wsdl"
);

// executer la methode connectNotation
$_SESSION["ETKBAtoken"] = $clientSOAP->connectNotation(
    $_POST["Email"],
    hash("sha256", utf8_encode($_POST["Password"]))
);
