<?php
// determiner les données utilisateurs $email, et $password

// désactiver le cache lors de la phase de test
if (isset($_POST["Connect"])) {
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue_sandbox.wsdl"
    );

    // executer la methode connectWeb
    $link = $clientSOAP->connectWeb(
        $email,
        hash("sha256", utf8_encode($password))
    );
}
