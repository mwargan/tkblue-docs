<?php
// determiner les données utilisateurs $email,$password

// première étape : obtenir le token
if (isset($_POST["ETKBAconnect"])) {
    // désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue_sandbox.wsdl"
    );

    // executer la méthode connectNotation
    $_SESSION["ETKBAtoken"] = $clientSOAP->connectNotation(
        $_POST["Email"],
        hash("sha256", utf8_encode($_POST["Password"]))
    );
}

// deuxième étape : déterminer les critères de recherche et télécharger les prestations concernées
if (isset($_POST["export"])) {
    // désactiver le cache lors de la phase de test
    $options = $_POST["option"];
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue_sandbox.wsdl"
    );
    $linenumber = 5;
    $pagenumber = 1;
    $options =
        $_POST["option"] .
        "&linenumber=" .
        $linenumber .
        "&pagenumber=" .
        $pagenumber;
    $retourexport = $clientSOAP->downloadChargeurDeclaration(
        $_POST["Email"],
        $_SESSION["ETKBAtoken"],
        $_POST["option"]
    );
    $result = json_decode($retourexport);
    $data = $result->data;

    // tester si la pagination est nécessaire
    if ($result->total > count($data)) {
        do {
            $pagenumber++;
            $options =
                $_POST["option"] .
                "&linenumber=" .
                $linenumber .
                "&pagenumber=" .
                $pagenumber;
            $retourexport = $clientSOAP->downloadChargeurDeclaration(
                $_POST["Email"],
                $_SESSION["ETKBAtoken"],
                $_POST["option"]
            );
            $result = json_decode($retourexport);
            $data = $result->data;
        } while (count($data) == $linenumber);
    }
}
