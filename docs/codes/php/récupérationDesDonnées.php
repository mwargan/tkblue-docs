<?php
// determiner les données utilisateurs $email,$password
if (isset($_POST["ETKBAconnect"])) {
    // désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue.wsdl"
    );

    // première étape : obtenir le token

    // executer la methode connectNotation
    $login = $_POST["LoginEmail"];
    $pwd = hash("sha256", utf8_encode($_POST["Pwd"]));
    try {
        $link = $clientSOAP->connectNotation($login, $pwd);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }

    // deuxième étape : déterminer les critères de recherche et lancer l'interrogation

    // gestion de la pagination sur getCarrierStatistics
    $linenumber = 20;
    $pagenumber = 1;
    $options = $_POST["option"] . "&linenumber=" . $linenumber;
    try {
        $res = $clientSOAP->getCarrierStatistics($login, $link, $options);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }
    $retour = json_decode($res);
    $data = $retour->data;

    if ($retour->total > count($data)) {
        do {
            $pagenumber++;
            $options =
                $_POST["option"] .
                "&linenumber=" .
                $linenumber .
                "&pagenumber=" .
                $pagenumber;
            $res = $clientSOAP->getCarrierStatistics($login, $link, $options);
            $retour = json_decode($res);
            $data = $retour->data;

            // gérer le contenu retourné dans $data
        } while (count($data) == $linenumber);
    }
}
