<?php
// determiner les données utilisateurs $email,$password
if (isset($_POST["ETKBAconnect"])) {
    // première étape : désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue_sandbox.wsdl"
    );

    // executer la méthode connectNotation
    $_SESSION["ETKBAtoken"] = $clientSOAP->connectNotation(
        $_POST["Email"],
        hash("sha256", utf8_encode($_POST["Password"]))
    );
}

if (isset($_POST["export"])) {
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue_sandbox.wsdl"
    );
    $retourimport = $clientSOAP->sendChargeurDeclaration(
        $_POST["Email"],
        $_SESSION["ETKBAtoken"],
        $_POST["option"],
        $_POST["list"]
    );
}
