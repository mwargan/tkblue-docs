# SOAP docs

::: tip
Voir aussi : généré automatiquement [PHP Swagger Docs](/php-reference)
:::

Cette page détaille les procédures d’échange de données automatique entre le système d’information d’un Chargeur ou Organisateur de transport et la plateforme TK’Blue.

Une fois l’échange de données programmé, il est recommandé d’effectuer des tests sur le site Sandbox de TK’Blue : [https://sandbox-notation.tkblueagency.com](https://sandbox-notation.tkblueagency.com). Les identifiants sont fournis par l’équipe TK’Blue.

La phase de tests terminée, il faut remplacer dans le code l’adresse : https://sandbox-notation.tkblueagency.com par : [https://www-notation.tkblueagency.com](https://www-notation.tkblueagency.com) et `tkblue_sandbox.wsdl` par `tkblue.wsdl`.

L’API TK’Blue est développée en utilisant la technologie Soap Client/Server (Simple Object Access Protocol).

Consulter [https://fr.wikibooks.org/wiki/Programmation_PHP/Exemples/webService](https://fr.wikibooks.org/wiki/Programmation_PHP/Exemples/webService) pour des explications de programmation, et [https://soapclient.com/soaptest.html](https://soapclient.com/soaptest.html) pour tester le serveur.

Retrouvez sur ce lien toutes les fonctions de l’API TK’Blue : [/php-reference/#documentation-for-models](/php-reference/#documentation-for-models)

## Authentification et autorisation

### Identification avec récupération d’un jeton

Le jeton d’authentification est obtenu en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models).
Cette fonction a deux paramètres en entrée, l’email du membre et son mot de passe crypté sha2\. Cette fonction retourne un jeton d’authentification à utiliser par la suite dans les appels des autres fonctions.

#### Paramètres en entrée

| Nom du champ | Type   | Description                                                    |
| ------------ | ------ | -------------------------------------------------------------- |
| Email        | String | Identifiant de connexion du membre sur la plateforme           |
| Mot de passe | String | Encryptage sha2 du mot de passe de connexion sur la plateforme |

#### Paramètres en sortie

Le champs « jeton d’authentification » est une chaine de caractères à utiliser dans l’appel de [**sendChargeurDeclaration**](/php-reference/#documentation-for-models)

#### Exemple de code

@[code](./../codes/php/identificationAvecRécupérationDUnJeton.php)

## Pagination des résultats

Le paramètre « total » retourné par la fonctions downloadChargeurDeclaration, getCarrierList, et getCarrierStatistics permet de gérer une pagination. Lorsque celui-ci est supérieur à 100, il est impératif de préciser le numéro de page à retourner et de procéder à des appels successifs de la fonction en incrémentant ce numéro de page jusqu’à épuisement.

## Erreurs

| Message                            | Code   | Description                                                      |
| ---------------------------------- | ------ | ---------------------------------------------------------------- |
| wrong cotisation rights            | 200 OK | Votre plan d'abonnement ne vous permet pas de faire cette action |
| unauthorized:Identifiant incorrect | 200 OK | Identifiant incorrect                                            |
| wrong identification               | 200 OK | Identifiant incorrect                                            |
| incorrect transport mode           | 200 OK | Fonction `getCarrierStatistics` il manque le paramètre `option`  |
| error                              | 200 OK | erreur générale                                                  |

## Importer des flux dans la plateforme TK’Blue

La procédure d’importation se déroule en 3 étapes :

1. Identification avec récupération d’un jeton (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton))
2. Envoi des données
3. Récupération des erreurs éventuelles

Il faut ensuite appeler la fonction [**sendChargeurDeclaration**](/php-reference/#documentation-for-models) avec l’email précédemment fourni, le jeton d’authentification retourné , une chaine de caractères pour décrire les options de transfert choisies et une chaine de caractères contenant les prestations de transport à importer dont le format dépend des options choisies.

Une fois les données importées, un retour est donné sur leur qualité, en fournissant le nombre de flux importés (Importer) et le nombre de flux valides (Valid). Si le nombre retourné « Imported » n’est pas égal au nombre « Valid », cela signifie que des prestations ont été importées de façon incomplète. Il ne sera pas possible de prendre en compte ces flux. Ces erreurs peuvent être corrigées sur la plateforme, mais il est prévu d’obtenir des renseignements en appelant la fonction [**getLastErrorList**](/php-reference/#documentation-for-models).

### Envoi des données

Une fois le jeton d’authentification obtenu (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton)) en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models), il faut appeler la fonction [**sendChargeurDeclaration**](/php-reference/#documentation-for-models).

Cette fonction a 4 paramètres en entrée : l’email du membre, le jeton d’authentification, un champ d’options de transfert et un champ prestations de transport. Elle indique en retour le nombre de prestations correctement importées.

#### Paramètres en entrée

| Nom du champ             | Type   | Description                                          |
| ------------------------ | ------ | ---------------------------------------------------- |
| Email                    | String | Identifiant de connexion du membre sur la plateforme |
| Jeton d’authentification | String | Retourné par la fonction ConnectNotation             |
| Options de transfert     | String | Détaillé ci-dessous                                  |
| Prestations de transport | String | Détaillé ci-dessous, au format CSV                   |

Le champ **« options de transfert »** est une chaine de caractères qui sera analysée par un appel PHP de type PARSE QUERY, c’est à dire qu’elle se compose d’une ou plusieurs options de la forme « nom_option1=valeur_option1&nom_option2=valeur_option2…. »

Les options possibles sont décrites ci-dessous :

| Nom du champ   | Type   | Description                                                                                                                                                                                                                                                                                                  | Valeurs              |
| -------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------- |
| csvDelimiter   | String | Indication du caractère délimitant les champs, par défaut le point-virgule est utilisé                                                                                                                                                                                                                       |                      |
| csvHeader      | Int    | Valeur par défaut 0 (les entêtes de colonnes ne sont pas à inclure), sinon indiquer 1 si elles le sont                                                                                                                                                                                                       | 0 ou 1               |
| fileFormatName | String | Nom du format d’exportation préalablement créé dans l’espace TK’Blue et permettant de préciser un ordre différent pour les colonnes, des unités spéciales et des champs de données complémentaires (en particulier Origine et Destination)                                                                   |                      |
| uniqueRef      | Int    | Indication de la présence d’une référence unique et utilisation de la fonction Dédoublonnage. Valeur par défaut 1 : chaque flux possède une référence unique et il faut procéder au dédoublonnage lorsque plusieurs flux possèdent la même référence. Sinon, valeur 0 pour ne pas procéder au dédoublonnage. | 0 ou 1               |
| year           | Int    | Indication de l’année de notation concernée. Valeur par défaut: l’année en cours au moment de l’importation des flux. Pour importer des flux sur une autre année, il faut indiquer l’année correspondante.                                                                                                   | 2015 ou 2016 ou 2017 |
| postponed      | Int    | Paramètre obsolète, toujours considéré égal à 1 afin de permettre le stockage des flux pour un retour de fonction plus rapide, le traitement est alors fait en différé.                                                                                                                                      | 0 ou 1               |

**Exemples :**

- csvDelimiter=,
- csvDelimiter=,&fileFormatName=nomformat

**Chaîne de caractères contenant les prestations de transport**

Si l’option fileFormatName n’est pas fournie, le CSV doit contenir dans l’ordre les colonnes suivantes :

| Nom du champ         | Nature | Type             | Description                                                       | Valeurs                                                                                                              |
| -------------------- | ------ | ---------------- | ----------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| Ref                  | O      | String (20c max) | Référence interne au TMS identifiant un segment d’une prestation  | Ex : "C13123-002“                                                                                                    |
| idTransporteur       | M      | String           | Numéro de TVA intracommunautaire\*                                | Ex : "FR01378901946"                                                                                                 |
| idModality           | C      | String           | Intitulé du mode de transport\*                                   | "Urban Road" , "Interurban Road" , "Rail" , “River" , "Short Sea Shipping" , "Deep Sea" , "Air" , "Forwarding Agent" |
| idFlotteTransporteur | C      | String           | Identifiant de la flotte du transporteur\*                        | Ex : "FPTRANPO_90_1"                                                                                                 |
| FlotteSpec           | M      | String           | Spécificité de la flotte                                          | "ou "Reefer"                                                                                                         |
| DateTransport        | M      | String YYY-MM-DD | Date de la prestation                                             |                                                                                                                      |
| WeightPos            | M      | Int, en kg       | poids                                                             |                                                                                                                      |
| KmPos                | C      | Int, en km       | Distance                                                          |                                                                                                                      |
| CO2                  | O      | Int, en gCO2     | Information CO2                                                   |                                                                                                                      |
| CO2Level             | O      | Int              | Niveau de l’information CO2                                       | De 1 à 4                                                                                                             |
| RefAgr               | O      | String (20c max) | Référence interne au TMS identifiant une prestation multi-segment | Ex: "C13123"                                                                                                         |

**_Nature des champs_**

Les champs dont la nature est M (mandatory) sont obligatoires. L’absence d’un champ de cette nature ou une valeur null entrainera le traitement en erreur de la prestation.

Ceux dont la nature est C (correspondance) peuvent être omis si le paramétrage des correspondances permet de les déterminer. Dans le cas contraire, l’absence d’un champ de cette nature ou une valeur null entrainera le traitement en erreur de la prestation.

Ceux dont la nature est O (optional) peuvent être absents ou renseignés par une valeur null, sans entrainer un traitement en erreur.

Les champs marqués d’un astérisque (\*) peuvent être renseignés à l’aide des paramétrages des correspondances.

**_Information CO<sub>2</sub>_**

Les champs concernant le CO<sub>2</sub> sont à renseigner uniquement si l’organisateur de transport ou le chargeur désire gérer lui-même le calcul de l’information CO<sub>2</sub>. Dans ce cas aucun calcul automatique d’information CO<sub>2</sub> ne sera fait.

Par contre, si des valeurs nulles sont transmises pour ces champs, l’information CO<sub>2</sub> et le niveau de l’information seront automatiquement calculés en fonction des informations récoltées par les transporteurs exécutant les prestations de transport.

_**Références internes**_

Les champs Ref et RefAgr sont à traiter différemment selon que le client est un organisateur de transport ou un chargeur :

→ _Pour un organisateur de transport :_

- Le champ Ref correspond à la référence interne au TMS. Dans le cas d’une prestation comportant plusieurs segments, chaque segment doit faire l’objet d’une ligne de données séparée et doit donc posséder une référence propre et unique.
  Le champ RefAgr contient alors la référence interne au TMS commune à l’ensemble de la prestation. Lorsque la prestation de transport ne comporte qu’un seul segment, les champs Ref et RefAgr seront identiques.
- L’organisateur de transport devra communiquer la valeur de RefAgr à son donneur d’ordres pour que ce dernier puisse suivre son information CO<sub>2</sub>.

→ _Pour un chargeur :_

- Le champ Ref correspond à la référence interne au TMS. Dans le cas d’une prestation comportant plusieurs segments, si chaque segment est confié directement à des transporteurs, chaque segment doit faire l’objet d’une ligne de données séparée et doit donc posséder une référence propre et unique.
  Le champ RefAgr contient alors la référence interne au TMS commune à l’ensemble de la prestation. Lorsque la prestation de transport ne comporte qu’un seul segment et est confiée directement à un transporteur, les champs Ref et RefAgr seront identiques.
- Dans le cas d’une prestation confiée à un organisateur de transport, l’éclatement éventuel de la prestation en plusieurs segments est géré dans le flux de données de l’organisateur de transport.
  Le champ RefAgr doit contenir la référence communiquée par l’organisateur de transport au chargeur afin de lui permettre de faire le suivi de son information CO<sub>2</sub>.

_**Mode de transport**_

Cas particulier du chargeur confiant une prestation à un organisateur de transport

Le chargeur peut choisir de préciser « Forwarding Agent » comme mode de transport, il n’a alors pas besoin de préciser l’identifiant de la flotte utilisée.
Ce sera en particulier le cas lorsque la prestation confiée à l’organisateur de transport sera éclatée en plusieurs segments dans son flux interne, ces segments pouvant correspondre à des modes de transports différents, et à des transporteurs et identifiants de flottes différents.

Ce sera encore le cas pour une prestation mono-segment, pour laquelle le chargeur laisse l’organisateur de transport entièrement libre du choix du mode de transport le plus adapté.

_**Paramètres d’appel**_

Dans la chaine CSV, chaque prestation doit être séparée par un caractère de fin de ligne et doit contenir les informations décrites précédemment séparées par le caractère défini dans l’option csvDelimiter.

Lorsque le paramètre fileFormatName est utilisé, les différents champs doivent être ordonnés selon le format d’importation précisé par ce paramètre. De même, les unités utilisées doivent correspondre à celles définies dans le format d’importation précisé.

#### Paramètres en sortie

Le retour de la fonction[ **sendChargeurDeclaration**](/php-reference/#documentation-for-models) est une chaîne au format json contenant, pour des raisons de compatibilité avec les versions précédentes, le total des prestations importées (complètes ou incomplètes), le total des prestations rejetées ainsi que le total des prestations importées sans informations manquantes.
Remarque: le traitement étant toujours effectué en différé, le total des prestations importées sans informations manquantes sera artificiellement égal au total des prestations importées.

#### Exemple de code

L’importation peut être testée à partir d’une page web contenant le code php suivant à titre d’exemple :

@[code](./../codes/php/envoiDesDonnées.php)

**Exemple de contenu de la variable $\_POST[‘list’] avec le choix du format CSV:**

« 4A », »FR01378901946″,"Routier Urbain","FPgoodgnv-105-1","","2013-04-07",10, 40, "","","4"
"4B","FR01378901946","Routier Urbain","FPTRANPO-90-1","","2013-04-04",10,40, "1203","3","4"

**Exemple de la chaine retournée :**

```json
[{ "Imported": 2, "Rejected": 0, "Valid": 2 }]
```

### Gestion des traitements différés

La fonction [**getPostponedImportStatus**](/php-reference/#documentation-for-models) a 3 paramètres en entrée : l’email du membre, le jeton retourné par la fonction de connexion et l’identifiant du traitement différé, et retourne une chaine de caractères au format JSON, indiquant le statut du traitement différé et la date et heure correspondant à ce statut. L’identifiant de traitement différé à fournir est celui qui a été retourné précédemment quand l’option de transfert postponed=1 a été utilisée.

#### Paramètres en entrée

| Nom du champ                      | Type   | Description                                          |
| --------------------------------- | ------ | ---------------------------------------------------- |
| Email                             | String | Identifiant de connexion du membre sur la plateforme |
| Jeton d’authentification          | String | Retourné par la fonction ConnectNotation             |
| Identifiant du traitement différé | Int    | Retourné par la fonction sendChargeurDeclaration     |

#### Paramètres en sortie

Le retour de la fonction [**getPostponedImportStatus**](/php-reference/#documentation-for-models) est une chaine au format json contenant:

→ Status: un texte parmi:

- Import file format needs to be chosen: uniquement pour les dépôts de fichiers hors utilisation du webservice
- First record needs to be checked: uniquement pour les dépôts de fichiers hors utilisation du webservice
- Ready for process: C’est l’état dans lequel se trouve le traitement différé généré par webservice juste après la fonction sendChargeurDeclaration
- Processing: la première étape du traitement différé est en cours
- First pass import done: la deuxième étape du traitement différé est en cours
- Import done: le traitement différé est terminé

→ StatusDate: la date et heure correspondant au statut retourné

Exemple de retour de la fonction [**getPostponedImportStatus**](https://test.tkblueagency.com/api/reference/)

```json
{ "Status": "Import done", "StatusDate": "2016-10-11 22:31:21" }
```

### Gestion des erreurs

La fonction [**getLastErrorList**](/php-reference/#documentation-for-models) a 2 paramètres en entrée : l’email du membre et le jeton retourné par la fonction de connexion, et retourne une chaine de caractères au format JSON, indiquant d’une part le nombre de prestations en erreur lors de la dernière importation, et d’autre part la liste des erreurs détectées. Le traitement d’une importation étant effectué en différé, il est recommandé de ne pas appeler cette fonction immédiatement après l’envoi de données.

#### Paramètres en entrée

| Nom du champ             | Type   | Description                                          |
| ------------------------ | ------ | ---------------------------------------------------- |
| Email                    | String | Identifiant de connexion du membre sur la plateforme |
| Jeton d’authentification | String | Retourné par la fonction ConnectNotation             |

#### Paramètres en sortie

Le retour de la fonction [**getLastErrorList**](/php-reference/#documentation-for-models) est une chaine au format json contenant:

→ Error: le nombre de prestations importées posant un problème,
→ List: chaine au format json contenant le détail d’une erreur:

- Field: nom du champ
- Comment: description de l’erreur
- Value: valeur du champ
- N: nombre de fois où l’erreur a été rencontré

Il peut y avoir plus d’erreurs détectées que de prestations en erreur puisqu’une même prestation peut avoir été mise en erreur pour plusieurs raisons.
Pour chaque type d’erreur retourné, on trouve le nom du champ incriminé, un commentaire expliquant le rejet, la valeur erronée transmise ainsi que le nombre de prestations concernées par cette erreur.

#### Exemples de code

**Exemple de prestations mal importées**

"4E","01378901946","Routier Urbain","FPgoodgnv-105-1","","2013-04-07","10", "40"
"1E","FR01378901946","Routier Urbain","FPTRANPO-90-1","","2013-04-04","10T","40"

→ "01378901946" n’est pas un numéro de TVA correct
→ "10T" n’est pas un champ numérique et ne devrait pas contenir l’unité

**Exemple de retour de la fonction [getLastErrorList](/php-reference/#documentation-for-models)**

```json
{
  "Error": "2",
  "List": [
    {
      "Field": "Weight",
      "Comment": "Poids non num\u00e9rique",
      "Value": "10T",
      "N": "1"
    },
    {
      "Field": "FlotteLabel",
      "Comment": "Flotte sans transporteur",
      "Value": "FPgoodgnv-105-1",
      "N": "1"
    },
    {
      "Field": "VatNumberString",
      "Comment": "Transporteur inconnu",
      "Value": "01378901946",
      "N": "1"
    }
  ]
}
```

## Exporter des flux depuis la plateforme TK’Blue

La procédure d’exportation se déroule en 2 étapes :

- Identification avec récupération d’un jeton (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton))
- Récupération des données,

Il faut ensuite appeler la fonction [**downloadChargeurDeclaration**](/php-reference/#documentation-for-models) avec l’email précédemment fourni, le jeton d’authentification retourné et une chaine de caractères pour décrire les options de transfert choisies.

### Récupération des données

Une fois le jeton d’authentification obtenu (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton)) en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models) il faut appeler la fonction [**downloadChargeurDeclaration**](/php-reference/#documentation-for-models).
Cette fonction a 3 paramètres en entrée, l’email du membre, le jeton d’authentification et un champ d’option de transfert. Elle retourne les informations concernant les prestations sélectionnées.

#### Paramètres en entrée

| Nom du champ             | Type   | Description                                          |
| ------------------------ | ------ | ---------------------------------------------------- |
| Email                    | String | Identifiant de connexion du membre sur la plateforme |
| Jeton d’authentification | String | Retourné par la fonction ConnectNotation             |
| Options de transfert     | String | Détaillé ci-dessous                                  |

Le champ **« options de transfert »** est une chaine de caractères qui sera analysée par un appel PHP de type PARSE QUERY, c’est à dire qu’elle se compose d’une ou plusieurs options de la forme « nom_option1=valeur_option1&nom_option2=valeur_option2…. » et d’un ou plusieurs critères de recherche

Les options possibles sont décrites ci-dessous :

**Les options de format**
Les options de format possibles sont :

| Nom du champs  | Type   | Description                                                                                                                                                                                                                                                                                                                                      | Valeurs                      |
| -------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------- |
| dataType       | String | Si ce paramètre n’est pas précisé, le format JSON sera utilisé                                                                                                                                                                                                                                                                                   | JSON ou CSV                  |
| csvDelimiter   | String | Uniquement pour l’option dataType=CSV. Indication du caractère délimitant les champs, par défaut le point-virgule est utilisé.                                                                                                                                                                                                                   |                              |
| csvHeader      | Int    | Uniquement pour l’option dataType=CSV. Valeur par défaut 0 : les entêtes de colonnes ne sont pas à inclure, sinon indiquer 1 si elles le sont.                                                                                                                                                                                                   | 0 ou 1                       |
| fileFormatName | String | Nom du format d’exportation préalablement créé dans l’espace chargeur et permettant dans le cas d’un envoi au format CSV de préciser un ordre différent pour les colonnes, des unités spéciales et des champs de données complémentaires (en particulier Origine et Destination)                                                                 |                              |
| pagenumber     | Int    | Numéro de la page dans l’ensemble des résultats de la recherche lorsqu’on utilise une recherche paginée. La plage de sélection se précise par un numéro de page et un nombre de résultats (linenumber) par page à retourner.                                                                                                                     | La valeur par défaut est 0   |
| linenumber     | Int    | Nombre maximal de prestations à retourner. La plage de sélection se précise par un numéro de page (pagenumber) et un nombre de résultats par page à retourner. La valeur par défaut est 100, c’est aussi la valeur maximale possible. Pour récupérer plus de 100 prestations vous devez programmer des appels successifs et gérer la pagination. | La valeur par défaut est 100 |

**Exemples :**

- dataType=CSV&csvDelimiter=,
- dataType=CSV&csvDelimiter=,&fileFormatName=nomformat
- dataType=CSV&csvDelimiter=,&pagenumber=2&linenumber=10

**Les critères de recherche**

Les critères de recherche correspondent à ceux disponibles sur la plateforme, dans l’historique des déclarations.

| Nom du champ | Type   | Correspondance            | Valeurs                                                                                                                                              |
| ------------ | ------ | ------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| Pb           | Int    | « Afficher seulement »    | 1 : Saisies valides 2 : Saisies incorrectes 3 : Modifications                                                                                        |
| minDate      | String | « Afficher seulement du » | ‘YYYY-MM-DD’                                                                                                                                         |
| maxDate      | String | « Afficher seulement au » | ‘YYYY-MM-DD’                                                                                                                                         |
| CO2          | Int    | » Suivi CO2«              | 0 : inconnu -1 : connu tout niveau 1 : niveau 1 2 : niveau 2 3 : niveau 3 4 : niveau 4                                                               |
| TKB          | Int    | « Suivi TK’T »            | 0 : inconnu -1 : connu tout niveau 1 : niveau Provisoire 2 : niveau Déclaré 3 : niveau Validé                                                        |
| VatNumber    | String | « Transporteur »          | Ex : « FR01378901946 » si l’identifiant est un numéro de TVA intracommunautaire, « 123456789B\|SG\|UEN » pour un identifiant de type UEN à Singapour |
| FieldLabel   | String | « Rechercher : champ »    | ‘Ref’, ‘RefAgr’, ‘Origin’, ‘Destination’                                                                                                             |
| FieldValue   | String | « Rechercher : valeur »   |                                                                                                                                                      |
| Year         | Int    | « Année »                 | y-2, y-1, y, y+1 où y est l’année en cours au moment du lancement de l’exportation                                                                   |

**Dépendance des critères**

Les critères minDate et maxDate sont à rapprocher du critère Pb, et correspondent aux dates de la prestation si le critère Pb est omis ou différent de 3\. Ils correspondent aux dates de modifications si le critère Pb vaut 3.

Exemples :

- Year=2017&VatNumber=FR89123456789|FR|VAT
- Year=2017&VatNumber=123456789B|SG|UEN
- Pb=2&minDate=2013-06-01&maxDate=2013-12-31
- Pb=3&minDate=2013-06-01&maxDate=2013-08-31

Le critère FieldValue est à rapprocher du critère FieldLabel. La sélection portera sur les prestations dont le champ FiledLabel prend la valeur FieldValue.
Exemples :

- FieldLabel=RefAgr&FieldValue=Man1
- FieldLabel=Origin&FieldValue=Nice

#### Paramètres en sortie

Le retour de la fonction **downloadChargeurDeclaration** est une chaine au format json contenant 2 paramètres :

| Nom du champs | Type | Description                                                                                                                                                                                    |
| ------------- | ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Total         | Int  | Le total des prestations concernées par les critères de recherche hors pagination                                                                                                              |
| data          | JSON | Une chaine JSON contenant les prestations retournées au format choisi (CSV ou JSON), chaque prestation comportant les champs sélectionnés par le format d’exportation indiqué dans les options |

```json
{
  "total": "18",
  "data": [
    {
      "Ref": "4A",
      "DateTransport": "2013-04-07",
      "idModality": "Routier Urbain",
      "idFlotteTransporteur": "FPgoodgnv-105- 1",
      "FlotteSpec": "",
      "Km": "400",
      "Weight": "10000",
      "CO2": "120",
      "CO2Level": "2",
      "RefAgr": "4",
      "idTransporteur": "FR01378901946",
      "Origin": null,
      "Destination": null
    },
    {
      "Ref": "7001-9153",
      "DateTransport": "2013-04-25",
      "idModality": "Routier Interurbain",
      "idFlotteTransporteur": "",
      "FlotteSpec": "",
      "Km": "12",
      "Weight": "97",
      "CO2": "0",
      "CO2Level": "0",
      "RefAgr": "7001-9153",
      "idTransporteur": "",
      "Origin": null,
      "Destination": null
    },
    {
      "Ref": "7645-10002",
      "DateTransport": "2013-04-27",
      "idModality": "Routier Interurbain",
      "idFlotteTransporteur": "",
      "FlotteSpec": "Frigo",
      "Km": "12",
      "Weight": "107",
      "CO2": "0",
      "CO2Level": "0",
      "RefAgr": "7645-10002",
      "idTransporteur": "",
      "Origin": null,
      "Destination": null
    },
    ...
  ]
}
```

[**supporte la pagination**](#pagination-des-resultats)

#### Exemple de code

L’exportation peut être testée à partir d’une page web contenant le code php suivant à titre d’exemple :

@[code](./../codes/php/ExporterFlux.php)

## Importer les paramétrages de correspondances transporteurs

La procédure d’importation se déroule en 2 étapes :

- Identification avec récupération d’un jeton (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton))
- Envoi des données.

Il faut ensuite appeler la fonction [**putCarrierCorrespondancyList**](/php-reference/#documentation-for-models) avec l’email précédemment fourni, le jeton d’authentification retourné , une chaine de caractères pour décrire les options de transfert choisies et une chaine de caractères contenant les informations sur les transporteurs à importer dont le format est prédéterminé.

Si le nombre retourné « Imported » n’est pas égal au nombre « Valid », cela signifie que des informations importées sont en erreur.

### Envoi des données

Une fois le jeton d’authentification obtenu (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton)) en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models) il faut appeler la fonction [**putCarrierCorrespondacyList**](/php-reference/#documentation-for-models).
Cette fonction a 4 paramètres en entrée, l’email du membre, le jeton d’authentification, un champ d’options de transfert et un champ Correspondance Transporteurs. Elle indique en retour le nombre de prestations correctement importées.

#### Paramètres en entrée

| Nom du champ                 | Type   | Description                                          |
| ---------------------------- | ------ | ---------------------------------------------------- |
| Email                        | String | Identifiant de connexion du membre sur la plateforme |
| Jeton d’authentification     | String | Retourné par la fonction ConnectNotation             |
| Options de transfert         | String | Détaillé ci-dessous                                  |
| Correspondance Transporteurs | String | Détaillé ci-dessous                                  |

Le champ **« options de transfert »** est une chaine de caractères qui sera analysée par un appel PHP de type PARSE QUERY, c’est à dire qu’elle se compose d’une ou plusieurs options de la forme « nom_option1=valeur_option1&nom_option2=valeur_option2…. ».

**Les options possibles sont décrites ci-dessous :**

| Nom du champ   | Type   | Description                                                                                                                                    | Valeurs     |
| -------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| dataType       | String | Si ce paramètre n’est pas précisé, le format JSON sera utilisé                                                                                 | JSON ou CSV |
| csvDelimiter   | String | Uniquement pour l’option dataType=CSV. Indication du caractère délimitant les champs, par défaut le point-virgule est utilisé.                 |             |
| csvHeader      | Int    | Uniquement pour l’option dataType=CSV. Valeur par défaut 0 : les entêtes de colonnes ne sont pas à inclure, sinon indiquer 1 si elles le sont. | 0 ou 1      |
| sendInvitation | Int    | Valeur par défaut 1 : des invitations sont à envoyer aux transporteurs pour lesquels un email et une raison sociale ont été précisés           | 0 ou 1      |

**Exemples :**

- dataType=CSV&csvDelimiter=,
- dataType=JSON

**Chaîne de caractères contenant les correspondances Transporteurs**

Lorsque le format choisi est JSON, pour chaque prestation, la structure json doit contenir les champs :

| Nom du champ         | Obligatoires       | Type   | Description                                                                                                                             | Valeurs                                                                                        |
| -------------------- | ------------------ | ------ | --------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| Name                 | :white_check_mark: | String | Libellé du transporteur tel que mentionné dans les flux                                                                                 | Ex : "TRPT Valognes » ou « F10XZ33 »                                                           |
| TIN                  | :white_check_mark: | String | Identifiant NIF du transporteur                                                                                                         | Ex : « FR01378901946 »                                                                         |
| idModality           |                    | String | Intitulé du mode de transport\*                                                                                                         | "Urban Road" , "Interurban Road" , "Rail" , “River" , "Short Sea" , "Air" , "Forwarding Agent" |
| idFlotteTransporteur |                    | String | Identifiant de la flotte du transporteur\*                                                                                              | Ex : "FPTRANPO_90_1"                                                                           |
| FlotteSpec           |                    | String | Spécificité de la flotte                                                                                                                | Vide ou "Reefer"                                                                               |
| idAdemeCO2           |                    | String | Référence de l’indice CO2 de niveau 1                                                                                                   | A\|30                                                                                          |
| Email                |                    | String | Email du contact chez le transporteur                                                                                                   | Ex : « transport@tkblueagency.com                                                              |
| LastName             |                    | String | Nom du contact chez le transporteur                                                                                                     | Ex : « DURAND »                                                                                |
| FirstName            |                    | String | Prénom du contact chez le transporteur                                                                                                  | Ex : « Pierre »                                                                                |
| BusinessName         |                    | String | Raison sociale du transporteur                                                                                                          | Ex : « Transports Valognes SARL »                                                              |
| idLanguage           |                    | Int    | Identifiant de langue du transporteur                                                                                                   | Ex: 1 pour Anglais, 2 pour Francais                                                            |
| CountryCode          |                    | String | Code Pays ISO. Ce code doit obligatoirement être précisé lorsqu’il n’est pas présent dans les deux premiers caractères du numéro de TVA | Ex: « FR » pour la France                                                                      |
| Acronyme TIN         |                    | String | Cet acronyme doit être précisé lorsque l’identifiant transporteur ne correspond pas à un numéro de TVA intracommunautaire               | Ex: « VAT » pour un numéro de TVA                                                              |

- Les champs marqués d’un astérisque (\*) peuvent être renseignés à l’aide des paramétrages des correspondances.

**_Information CO<sub>2</sub>_**

Le champ idAdemeCO2 permet d’affecter un indice CO<sub>2</sub> de niveau 1 par défaut au transporteur en attente de sa déclaration.

Une fois que le transporteur aura calculé son propre indice CO<sub>2</sub>, il remplacera automatiquement et sans action de l’utilisateur l’indice affecté par défaut.

**_Mode de transport_**

Pour paramétrer une correspondance transporteur qui opère sur plusieurs modes de transport avec une flotte spécifique pour chaque mode, il faut alors créer plusieurs paramétrages pour chaque mode de transport

**_Paramètres d’appel_**

Si le format choisi est CSV, chaque correspondance doit être séparée par un caractère de fin de ligne et doit contenir les informations décrites précédemment dans la structure JSON, séparées par le caractère défini par csvDelimiter.

Les différents champs doivent être ordonnés selon l’ordre des champs indiqué dans le tableau ci-dessus.

#### Paramètres en sortie

Le retour de la fonction[ **putCarrierCorrespondancyList**](https://dev.tkblueagency.com/api/reference/) est une chaine au format json contenant

| Nom du paramètre | Description                                                                                    |
| ---------------- | ---------------------------------------------------------------------------------------------- |
| Imported         | Le total des correspondances importées (complètes ou incomplètes)                              |
| Rejected         | Le total des correspondances rejetées                                                          |
| Incoherent       | Le total des incohérences rencontrées (mode de transport et identifiant de flotte par exemple) |
| Invitations      | Le total des invitations envoyées automatiquement par courrier électronique                    |
| Errors           | La liste des erreurs rencontrées au format json.                                               |

#### Exemple de code

L’importation peut être testée à partir d’une page web contenant le code php suivant à titre d’exemple :

@[code](./../codes/php/envoiDesDonnées.php)

**Exemple de contenu de la variable $\_POST[‘list’] avec le choix du format JSON:**

```json
[
  { "Name": "WSTR1", "TIN": "FR62421868084" },
  {
    "Name": "DONAVIN",
    "TIN": "06141912850013",
    "idModality": "Urban Road",
    "Email": "abcd@xyz.fr",
    "FirstName": "Paul",
    "LastName": "DUPONT",
    "BusinessName": "SARL DUPONT",
    "idLanguage": 2,
    "CountryCode": "SV",
    "TINAcr": "NIT"
  }
]
```

**Exemple de contenu de la variable $\_POST[‘list’] avec le choix du format CSV:**

```csv
"WSTR1","FR62421868084"
"DONAVIN","06141912850013","Urban Road",,,,"abcd@xyz.fr","Paul","DUPONT","SARL DUPONT",2,"SV","NIT"
```

**Exemple de la chaine retournée :**

```json
{
  "Imported": 0,
  "Rejected": 3,
  "Incoherent": 0,
  "Invitation": 0,
  "Errors": [
    "line o:InvalidVATNumber",
    "line 1:InvalidVATNumber",
    "line 2:InvalidVATNumber"
  ]
}
```

## Interroger la Blue Gallery

La procédure d’interrogation se déroule en 2 étapes :

- Identification avec récupération d’un jeton (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton))
- Récupération des données.

Il faut ensuite appeler la fonction [**getCarrierList**](/php-reference/#documentation-for-models) avec l’email précédemment fourni, le jeton d’authentification retourné et une chaine de caractères pour décrire les options de transfert choisies.

### Paramètres en entrée

| Nom du champ             | Type   | Description                                          |
| ------------------------ | ------ | ---------------------------------------------------- |
| Email                    | String | Identifiant de connexion du membre sur la plateforme |
| Jeton d’authentification | String | Retourné par la fonction ConnectNotation             |
| Options de transfert     | String | Détaillé ci-dessous                                  |

Le champ « options de transfert » est une chaine de caractères qui sera analysée par un appel PHP de type PARSE QUERY, c’est à dire qu’elle se compose d’une ou plusieurs options de la forme « param1=val1&param2=val2…. »

## Interroger la Blue Gallery

### Récupération des données

Une fois le jeton d’authentification obtenu (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton)) en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models) il faut appeler la fonction [**getCarrierList**](/php-reference/#documentation-for-models).
Cette fonction a 3 paramètres en entrée : l’email du membre, le jeton d’authentification et un champ d’options de transfert. Elle retourne les informations concernant les transporteurs sélectionnés.

#### Paramètres en entrée

**Les options possibles sont décrites ci-dessous :**

| Nom du champs | Type    | Description                                                                                                                                                                                                                                                                                                                                   | Valeur                                                                                                                                             |
| ------------- | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| pagenumber    | Int     | Numéro de la page dans l’ensemble des résultats de la recherche lorsqu’on utilise une recherche paginée. La plage de sélection se précise par un numéro de page et un nombre de résultats (linenumber) par page à retourner.                                                                                                                  | La valeur par défaut est 0                                                                                                                         |
| linenumber    | Int     | Nombre maximal de prestations à retourner. La plage de sélection se précise par un numéro de page (pagenumber) et un nombre de résultats par page à retourner. La valeur par défaut est 100, c’est aussi la valeur maximale possible. Pour récupérer plus de 100 prestations il faut programmer des appels successifs et gérer la pagination. | La valeur par défaut est 100                                                                                                                       |
| myScopeOnly   | Boolean | Détermination des transporteurs interrogés : l’ensemble des transporteurs ou seulement les transporteurs mentionnés dans ses flux.                                                                                                                                                                                                            | « Faux » par défaut. « Vrai » pour restreindre à ses propres transporteurs                                                                         |
| Modality      | String  | Intitulé du mode de transport                                                                                                                                                                                                                                                                                                                 | « Urban Road » , « Interurban Road » , « Rail » , « River » , « Short Sea Shipping » , « Deep Sea » , « Air »                                      |
| BusinessName  | String  | Raison sociale du transporteur                                                                                                                                                                                                                                                                                                                |                                                                                                                                                    |
| VatNumber     | String  | NIF du transporteur                                                                                                                                                                                                                                                                                                                           | Ex « FR62421868084 » si l’identifiant est un numéro de TVA intracommunautaire, « 123456789B\|SG\|UEN » pour un identifiant de type UEN à Singapour |
| GHG           | String  | Choix de l’affichage GES                                                                                                                                                                                                                                                                                                                      | FR (défaut)/ EN-Gp / EN-Gr                                                                                                                         |

**Exemples :**

- Modality=Rail&VatNumber=FR09123456789,
- Modality=Air&VatNumber=ABC123456UV4|MX|RFC
- Modality=URBAN ROAD&GHG=EN-Gr
- GHG=FR&pagenumber=2&linenumber=10

#### Paramètres en sortie

Le retour de la fonction **getCarrierList** est une chaine au format json contenant 2 paramètres :

| Nom du champ | Type | Description                                                                                                                                                                                                                                                           |
| ------------ | ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Total        | Int  | Le nombre total de transporteurs concernés par les critères de recherche hors pagination                                                                                                                                                                              |
| data         | JSON | Une chaine JSON contenant les informations suivantes pour chaque transporteur sélectionné: BusinessName (raison sociale), VatNumberString (NIF du transporteur), AvgIT (indice TK’T moyen), AvgICO2 (indice CO2/GES moyen selon l’unité choisie par le paramètre GHG) |

```json
{
  "total": "28",
  "data": [
    {
      "BusinessName": "AGGREGATEUR TEST",
      "VatNumberString": "FR00521890939",
      "AvgIT": 3.2,
      "AvgICO2": 750.4
    },
    ...
  ]
}
```

[**supporte la pagination**](#pagination-des-resultats)

#### Exemple de code

```php
// determiner les données utilisateurs $email,$password
if (isset($_POST["ETKBAconnect"])) {
    // désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue.wsdl"
    );

    // première étape : obtenir le token

    // executer la méthode connectNotation
    $login = $_POST["LoginEmail"];
    $pwd = hash("sha256", utf8_encode($_POST["Pwd"]));
    try {
        $link = $clientSOAP->connectNotation($login, $pwd);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }

    // deuxième étape : déterminer les critères de recherche et lancer l'interrogation

    // gestion de la pagination sur getCarrierList
    $linenumber = 5;
    $pagenumber = 1;
    $options = $_POST["option"] . "&linenumber=" . $linenumber;
    try {
        $res = $clientSOAP->getCarrierList($login, $link, $options);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }
    $retour = json_decode($res);
    $data = $retour->data;

    if ($retour->total > count($data)) {
        do {
            $pagenumber++;
            $options =
                $_POST["option"] .
                "&linenumber=" .
                $linenumber .
                "&pagenumber=" .
                $pagenumber;
            $res = $clientSOAP->getCarrierList($login, $link, $options);
            $retour = json_decode($res);
            $data = $retour->data;

            // gérer le contenu retourné dans $data
        } while (count($data) == $linenumber);
    }
}

```

La procédure d’interrogation se déroule en 2 étapes :

- Identification avec récupération d’un jeton (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton))
- Récupération des données.

Le jeton d’authentification est obtenu en appelant la fonction [**connectNotation**](https:/dev.tkblueagency.com/api/reference/) et en fournissant l’email du membre et son mot de passe crypté sha1\. Cette fonction retourne un jeton d’authentification.

Il faut ensuite appeler la fonction [**getCarrierPerformance**](/php-reference/#documentation-for-models) avec l’email précédemment fourni, le jeton d’authentification retourné et une chaine de caractères pour décrire les options de transfert choisies.

## Interroger les performances d'un transporteur

### Récupération des données

Une fois le jeton d’authentification obtenu (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton)) en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models) il faut appeler la fonction [**getCarrierPerformance**](/php-reference/#documentation-for-models).
Cette fonction a 3 paramètres en entrée : l’email du membre, le jeton d’authentification et un champ d’options de transfert. Elle retourne les informations concernant le transporteur sélectionné.

#### Paramètres en entrée

**Les options possibles sont décrites ci-dessous :**

| Nom du champ | Type   | Description                                                                                                                                         | Valeur                                                                                                                                                                                                 |
| ------------ | ------ | --------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| VatNumber    | String | NIF du transporteur.                                                                                                                                | Pas de valeur par défaut, ce champ est obligatoire. Ex « FR62421868084 » si l’identifiant est un numéro de TVA intracommunautaire, « 123456789B\|SG\|UEN » pour un identifiant de type UEN à Singapour |
| Modality     | String | Intitulé du mode de transport parmi : « Urban Road » , « Interurban Road » , « Rail » , « River » , « Short Sea Shipping » , « Deep Sea » , « Air » | La valeur par défaut est « Interurban Road ».                                                                                                                                                          |

**Exemples :**

- Modality=Rail&VatNumber=FR09123456789,
- Modality=Air&VatNumber=ABC123456UV4|MX|RFC
- VatNumber=FR09123456789

#### Paramètres en sortie

Le retour de la fonction **getCarrierPerformance** est une chaine au format json contenant 3 paramètres :

| Nom du champ | Type   | Description                                                                                                                                                                                                                                                                                                                                    |
| ------------ | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| BusinessName | String | La raison sociale correspondant au numéro de TVA intracommunautaire si ce transporteur est inscrit sur la plateforme TK’Blue                                                                                                                                                                                                                   |
| Accuracy     | String | La précision des informations retournées: « Default » si des performances par défaut sont retournées, soit parce que le transporteur n’est pas encore inscrit, soit parce qu’il n’a pas encore fait de déclaration de flotte dans le mode de transport recherché, « Carrier » si les performances retournées sont bien celles du transporteur. |
| List         | JSON   | Une chaine JSON contenant les clés « TKT », « CO2 » et « Euro » associant les indicateurs disponibles pour chaque flotte sélectionnée. Le contenu de ces 3 clés est détaillé ci-dessous                                                                                                                                                        |

**_TKT_**

Cette clé regroupe les caractéristiques de la flotte TK’Blue :

| Nom du champ         | Type    | Description                                                                                                      |
| -------------------- | ------- | ---------------------------------------------------------------------------------------------------------------- |
| idFlotteTransporteur | Int     | Identifiant interne de la flotte                                                                                 |
| FlotteLabel          | String  | Intitulé complet de la flotte (ce qui doit être communiqué dans les flux Chargeur ou Organisateur de transport). |
| DateUpdate           | Date    | Date de la création ou dernière mise à jour de la flotte au format YYYY-MM-DD                                    |
| Index                | Decimal | Valeur de l’indice TK’T de la flotte (compris entre 0 et 100).                                                   |
| FlotteCategory       | String  | Intitulé de la catégorie de flotte                                                                               |

**_CO2_**

Cette clé regroupe les caractéristiques de l’indice CO<sub>2</sub> associé par défaut à la flotte TK’Blue :

| Nom du champ | Type    | Description                                                                             |
| ------------ | ------- | --------------------------------------------------------------------------------------- |
| IndexFr      | Decimal | Indice CO2 en gCO2/t.km calculé conforme au décret français (du puits à la roue)        |
| IndexGr      | Decimal | Indice GES en gCO2e/t.km calculé conforme à la norme EN 16258 (du réservoir à la roue). |
| IndexGp      | Decimal | Indice GES en gCO2/t.km calculé conforme à la norme EN 16258 (du puits à la roue)       |
| Level        | Int     | Niveau de l’indice (selon la classification du décret français).                        |
| IndexRef     | String  | Référence de l’indice à préciser dans les flux Chargeur ou Organisateur de transport    |
| FlotteLabel  | String  | Intitulé complet de l’indice avec sa date de déclaration (si niveau> 1).                |

**_Euro_**

Cette clé regroupe les caractéristiques de la valorisation en coûts d’externalités négatives de la flotte TK’Blue et de son indice CO<sub>2</sub> associé :

| Nom du champ | Type    | Description                                                                                       |
| ------------ | ------- | ------------------------------------------------------------------------------------------------- |
| Cost         | Decimal | Coût total des externalités négatives en c€/t.km                                                  |
| TrustLevel   | Int     | Indice de fiabilité de l’association de l’indice CO2 à la flotte TK’Blue, compris entre 0 et 100. |

**Exemple de la chaine retournée :**

```json
{
  "List": [
    {
      "TKT": {
        "idFlotteTransporteur": "1998",
        "FlotteLabel": "OFValIRAvecNiv1-1998-2",
        "DateUpdate": "2017-02-09",
        "Index": 72.59,
        "FlotteCategory": "Road Interurban GVW \u2264 7.5t Gasoil"
      },
      "CO2": {
        "IndexFr": "750.444",
        "IndexGr": "620.889",
        "IndexGp": "774.889",
        "Level": "1",
        "IndexRef": "0|A|21",
        "FlotteLabel": "SuiviAssocN1 (09/02/2017)"
      },
      "Euro": { "Cost": 15.71, "TrustLevel": 75 }
    },
    ...
  ],
  "Accuracy": "Carrier",
  "BusinessName": "AVECSUIVI"
}
```

#### Exemple de code

```php
// determiner les données utilisateurs $email,$password
if (isset($_POST["ETKBAconnect"])) {
    // désactiver le cache lors de la phase de test
    ini_set("soap.wsdl_cache_enabled", "0");

    // lier le client au fichier WSDL
    $clientSOAP = new SoapClient(
        "https://sandbox-notation.tkblueagency.com/res/tkblue.wsdl"
    );

    // première étape : obtenir le token

    // executer la méthode connectNotation
    $login = $_POST["LoginEmail"];
    $pwd = hash("sha256", utf8_encode($_POST["Pwd"]));
    try {
        $link = $clientSOAP->connectNotation($login, $pwd);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }

    // deuxième étape : déterminer les critères de recherche et lancer l'interrogation
    $options = $_POST["option"];
    try {
        $res = $clientSOAP->getCarrierPerformance($login, $link, $options);
    } catch (Exception $e) {
        echo "Exception reçue : ", $e->getMessage(), "\n";
    }
    $retour = json_decode($res, true);

    // affichage des résultats
    echo "Accuracy :" .
        $retour["Accuracy"] .
        '
';
    echo "BusinessName :" . $retour["BusinessName"];

    $list = $retour["List"];
    foreach ($list as $l) {
    }
}
```

<table id="tableau1"   cellspacing="0" cellpadding="0">

<thead>

<tr>

<th colspan="4">

`TKT`

</th>

<th colspan="4">

`CO<sub>2</sub>`

</th>

<th colspan="2">

`TK'€`

</th>

</tr>

<tr>

<th>

`Flotte TK'T`

</th>

<th>

`Catégorie`

</th>

<th>

`Indice TK'T`

</th>

<th>

`Date`

</th>

<th>

`Flotte CO<sub>2</sub>`

</th>

<th>

`Indice CO<sub>2</sub>`

</th>

<th>

`Ref`

</th>

<th>

`Niveau`

</th>

<th>

`Coût total`

</th>

<th>

`Fiabilité`

</th>

</tr>

</thead>

<tbody>

<tr>

<td>

```php
 echo $l['TKT']['FlotteLabel'];
```

</td>

<td>

```php
 echo $l['TKT']['FlotteCategory'];
```

</td>

<td>

```php
 echo $l['TKT']['Index'];
```

</td>

<td>

```php
 echo $l['TKT']['DateUpdate'];
```

</td>

<td>

```php
 echo $l['CO2']['FlotteLabel'];
```

</td>

<td>

```php
 echo $l['CO2']['IndexFr'];
```

</td>

<td>

```php
 echo $l['CO2']['IndexRef'];
```

</td>

<td>

```php
 echo $l['CO2']['Level'];
```

</td>

<td>

```php
 echo $l['Euro']['Cost'];
```

</td>

<td>

```php
 echo $l['Euro']['TrustLevel'];
```

</td>

</tr>

</tbody>

</table>

La procédure d’interrogation se déroule en 2 étapes :

- Identification avec récupération d’un jeton
- Récupération des données.

Il faut ensuite appeler la fonction [**getCarrierStatistics**](/php-reference/#documentation-for-models) avec l’email précédemment fourni, le jeton d’authentification retourné et une chaine de caractères pour décrire les options de transfert choisies.

## Exporter les statistiques de ses transporteurs

### Récupération des données

Une fois le jeton d’authentification obtenu (voir: [Identification avec récupération d’un jeton](#identification-avec-recuperation-d-un-jeton)) en appelant la fonction [**connectNotation**](/php-reference/#documentation-for-models) il faut appeler la fonction [**getCarrierStatistics**](/php-reference/#documentation-for-models).
Cette fonction a 3 paramètres en entrée : l’email du membre, le jeton d’authentification et un champ d’options de transfert. Elle retourne les informations concernant la période sélectionnée.

#### Paramètres en entrée

**Les options possibles sont décrites ci-dessous :**

| Nom du champs | Type   | Description                                                                                                                                                                                                                                                                                                                               | Valeur                                                                                                                                               |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| Nom du champs | Type   | Description                                                                                                                                                                                                                                                                                                                               | Valeur                                                                                                                                               |
| pagenumber    | Int    | Numéro de la page dans l’ensemble des résultats de la recherche lorsqu’on utilise une recherche paginée. La plage de sélection se précise par un numéro de page et un nombre de résultats (linenumber) par page à retourner.                                                                                                              | La valeur par défaut est 0                                                                                                                           |
| linenumber    | Int    | Nombre maximal de résultats à retourner. La plage de sélection se précise par un numéro de page (pagenumber) et un nombre de résultats par page à retourner. La valeur par défaut est 100, c’est aussi la valeur maximale possible. Pour récupérer plus de 100 résultats il faut programmer des appels successifs et gérer la pagination. | La valeur par défaut est 100                                                                                                                         |
| Year          | Int    | L’année de statistiques.                                                                                                                                                                                                                                                                                                                  | y-2, y-1, y, y+1 où y est l’année en cours au moment du lancement de l’interrogation                                                                 |
| Month         | Int    | Permet de récupérer les résultats pour un mois précis d’une année, ou pour une année civile entière, ou encore pour une année fiscale entière; La période correspondant à une année fiscale est définie dans l’espace chargeur.                                                                                                           | O (valeur par défaut) pour les statistiques annuelles, 13 pour les statistiques annuelles sur année fiscale, 1 à 12 pour les statistiques mensuelles |
| Modality      | String | Intitulé du mode de transport. Cette donnée est obligatoire, les résultats ne pouvant pas être obtenus tous modes confondus.                                                                                                                                                                                                              | « Urban Road » , « Interurban Road » , « Rail » , « River » , « Short Sea Shipping » , « Deep Sea » , « Air »                                        |

**Exemples :**

- Modality=Rail&Year=2016,
- Modality=URBAN ROAD&Year=2017&Month=3

#### Paramètres en sortie

Le retour de la fonction **getCarrierStatistics** est une chaine au format json contenant 2 paramètres :

| Nom du champ | Type | Description                                                                                                                                                                                                                                                                        |
| ------------ | ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Total        | Int  | Le nombre total de transporteurs concernés par les critères de recherche hors pagination                                                                                                                                                                                           |
| data         | JSON | Une chaine JSON contenant les informations suivantes pour chaque transporteur sélectionné: BusinessName (raison sociale), VatNumberString (numéro de TVA intracommunautaire), AvgIT (indice TK’T moyen), AvgICO2 (indice CO2/GES moyen selon l’unité choisie par le paramètre GHG) |

Chaque item de la variable data retournée contient les informations suivantes :

| Nom du champ    | Type    | Description                                                                                                   |
| --------------- | ------- | ------------------------------------------------------------------------------------------------------------- |
| BusinessName    | String  | La raison sociale du transporteur                                                                             |
| VatNumberString | String  | Son NIF                                                                                                       |
| TKM             | Decimal | Le nombre total de tonnes.kilomètres opérées pour le compte du chargeur par ce transporteur                   |
| ITKT            | Decimal | Son indice moyen TK’T constaté dans les flux, compris entre 0 et 100                                          |
| COST            | Decimal | Le coût sociétal total en €                                                                                   |
| SCO2            | Decimal | le total des émissions CO2 selon le décret français (du puits à la roue) en kgCO2                             |
| SGp             | Decimal | le total des émissions GES selon la norme Européenne (du puits à la roue) en kgCO2e                           |
| SGr             | Decimal | le total des émissions GES selon la norme Européenne (du réservoir à la roue) en kgCO2e                       |
| ICO2            | Decimal | Son indice moyen CO2 constaté dans les flux, selon le décret français (du puits à la roue) en gCO2/t.km       |
| IGp             | Decimal | Son indice moyen GES constaté dans les flux, selon la norme Européenne (du puits à la roue) en gCO2e/t.km     |
| IGp             | Decimal | Son indice moyen GES constaté dans les flux, selon la norme Européenne (du réservoir à la roue) en gCO2e/t.km |

```json
{
  "total": "2",
  "data": [
    {
      "BusinessName": "INTERROUTEUN",
      "VatNumber": "FR02325625440",
      "TKM": "2000000.0000",
      "ITKT": 89.335,
      "COST": "3788.8880",
      "CO2": "131114.0000",
      "Gr": "108480.0000",
      "Gp": "135386.0000",
      "ICO2": 6.56,
      "IGr": 5.42,
      "IGp": 6.77
    },
    ...
  ]
}
```

[**supporte la pagination**](#pagination-des-resultats)

#### Exemple de code

@[code](../../codes/php/récupérationDesDonnées.php)

## Se connecter à la plateforme TK'Blue sans identification

Pour pouvoir se connecter de façon transparente sur les espaces TK’Blue dédiés Chargeurs ou Organisateurs de transport, c’est à dire sans saisie manuelle de l’identifiant et du mot de passe, il faut demander un jeton d’authentification.

Les jetons d’authentification ont une durée de vie limitée et deviennent inactifs dès qu’ils ont été utilisés, il est donc inutile de les stocker pour les réutiliser ultérieurement. Autrement dit, un jeton d’authentification doit être demandé pour chaque connexion.

### Identification

Le jeton d’authentification est obtenu en appelant la fonction [**connectWeb**](/php-reference/#documentation-for-models).
Cette fonction a 2 paramètres en entrée : l’email du membre et son mot de passe crypté sha2 et retourne un lien de redirection contenant le jeton d’authentification.

#### Paramètres en entrée

| Nom du champs | Type   | Description                                                    |
| ------------- | ------ | -------------------------------------------------------------- |
| Email         | String | Identifiant de connexion du membre sur la plateforme           |
| Mot de passe  | String | Encryptage sha2 du mot de passe de connexion sur la plateforme |

#### Paramètres en sortie

La fonction retourne un lien à utiliser pour accéder directement à l’espace membre sans passer par l’authentification.

#### Exemple de code

@[code](./../codes/php/identification.php)
