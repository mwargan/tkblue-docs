# Blade Components

## actions

| Param              | Type     | Default |
| ------------------ | -------- | ------- |
| `$show`            | `URL`    | -       |
| `$edit`            | `URL`    | -       |
| `$delete`          | `URL`    | -       |
| `$deleteComponent` | `string` | delete  |

## add-and-modify-select-filter

| Param       | Type                        | Default |
| ----------- | --------------------------- | ------- |
| `$function` | JavaScript onclick function | -       |
| `$select`   | HTML ID selector            | -       |

## backdrop-dialog-modal

| Param   | Type                                                 | Default |
| ------- | ---------------------------------------------------- | ------- |
| `$id`   | The ID of staticBackdrop\_                           | -       |
| `$wait` | The static backdrop label (translatable string code) | -       |

## check-box-with-text

| Param    | Type                                 | Default |
| -------- | ------------------------------------ | ------- |
| `$field` | The ID and name of the input         | -       |
| `$label` | The label (translatable string code) | -       |

## data-table-select

## data-table

## divider

## import-dialog-modal

## main-content-container-fluid

## modal

## notification

## option-with-selected

## pill-header-tag

## radio-with-text

## select-from-list

## select-make-list

| Param          | Type                                 | Default |
| -------------- | ------------------------------------ | ------- |
| `$values`      | `array`                              | -       |
| `$selected`    | The label (translatable string code) | -       |
| `$name`        | The label (translatable string code) | -       |
| `$translation` | The label (translatable string code) | -       |
| `$id`          | The label (translatable string code) | -       |

## table-edit
